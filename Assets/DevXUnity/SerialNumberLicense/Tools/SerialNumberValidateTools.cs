﻿using System;
using UnityEngine;

using System.IO;
using System.Xml;
using System.Security.Cryptography;



namespace DevXUnityTools
{
    #region SerialNumberValidateTools
    /// <summary>
    /// SerialNumber validation class
    /// </summary>
    internal class SerialNumberValidateTools
    {

        #region Verify
        /// <summary>
        /// Verify current user serial number
        /// </summary>
        /// <returns></returns>
        internal static bool Verify()
        {
            int? license_expired_after_num_days =null;

            return Verify(out license_expired_after_num_days );
        }
        #endregion


        #region Verify
        /// <summary>
        /// Verify current user serial number
        /// </summary>
        /// <returns></returns>
        internal static bool Verify(out int? license_expired_after_num_days)
        {
            license_expired_after_num_days = null;

            DateTime DateTime_Now = DateTime.UtcNow;
            bool TIME_valid = UpdateTime();


            var lic_file = Resources.Load<TextAsset>("SN-License-OpenKey");
            if (lic_file == null || string.IsNullOrEmpty(lic_file.text))
                return false;
            string open_key = lic_file.text;
            var signer = new SerialNumberVerify(open_key);

            if (string.IsNullOrEmpty(SerialNumberKey))
                return false;
            DateTime? expir_date=null;

            string hardwareID = HardwareID;
            String ser_num = SerialNumberKey.Trim();

            if (SerialNumberKey.Contains("-@"))
            {
                ser_num = SerialNumberKey.Substring(0, SerialNumberKey.IndexOf("-@")).Trim();
                string stick = SerialNumberKey.Substring(SerialNumberKey.IndexOf("-@") + "-@".Length).Trim();
                try
                {
                    expir_date = new DateTime(2000, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddDays(int.Parse(stick)).AddHours(23);
                }
                catch { }

                bool res = signer.VerifySignature(hardwareID, ser_num);

                if (res==false && expir_date.HasValue )
                {
                    if (expir_date.Value < DateTime_Now)
                    {
                        license_expired_after_num_days = 0;
                        return false;
                    }
                    else
                    {
                        license_expired_after_num_days = (int)((expir_date.Value - DateTime_Now).TotalDays);
                    }
                    if (TIME_valid)
                    {
                        if (res == false)
                            res = signer.VerifySignature("DateExpiration:" + expir_date.Value.ToString("yyyy.MM.dd"), ser_num);
                        if (res == false)
                            res = signer.VerifySignature(hardwareID + "DateExpiration:" + expir_date.Value.ToString("yyyy.MM.dd"), ser_num);
                    }
                }
                return res;
            }
            else
            {
                bool res = signer.VerifySignature(hardwareID, ser_num);

                //if (TIME_valid)
                //{
                //    int i = 0;
                //    while (res == false && i < 400)
                //    {
                //        if (res == false)
                //            res = signer.VerifySignature("DateExpiration:" + DateTime_Now.AddDays(i).ToString("yyyy.MM.dd"), ser_num);
                //        if (res == false)
                //            res = signer.VerifySignature(hardwareID + "DateExpiration:" + DateTime_Now.AddDays(i).ToString("yyyy.MM.dd"), ser_num);
                //        if (res)
                //            break;

                //        i++;
                //    }

                //    if (i > 0)
                //        license_expired_after_num_days = i;
                //}
                return res;
            }

            
        }
        #endregion



        #region UpdateTime
        static string reg_key = (Application.productName + "_dxserialnumber_").GetHashCode().ToString();
        static DateTime? last_time;
        static bool _TIME_valid = true;
        /// <summary>
        /// UpdateTime
        /// </summary>
        /// <returns></returns>
        internal static bool UpdateTime()
        {
            if (last_time.HasValue && (DateTime.UtcNow - last_time.Value).TotalMinutes < 1)
                return _TIME_valid;


            DateTime DateTime_Now = DateTime.UtcNow;
            last_time = DateTime_Now;

            _TIME_valid = true;

            String Stiks = PlayerPrefs.GetString(reg_key);
            if (string.IsNullOrEmpty(Stiks) == false)
            {
                var t_DateTime_Now = new DateTime(long.Parse(Stiks), DateTimeKind.Utc);
                if (DateTime_Now < t_DateTime_Now)
                    _TIME_valid = false;
            }

            if (_TIME_valid)
            {
                PlayerPrefs.SetString(reg_key, DateTime_Now.Ticks.ToString());
                PlayerPrefs.Save();
            }
            return _TIME_valid;
        }
        #endregion


        #region SerialNumberKey
        /// <summary>
        /// Set or get - User Serial Number key
        /// </summary>
        internal static string SerialNumberKey
        {
            set
            {
                UnityEngine.PlayerPrefs.SetString("SerialNumberKey", value);
                UnityEngine.PlayerPrefs.Save();
            }
            get
            {
                return UnityEngine.PlayerPrefs.GetString("SerialNumberKey");
            }
        }
        #endregion

        //

        #region HardwareID
        /// <summary>
        /// Hardware ID
        /// </summary>
        static internal string HardwareID
        {
            get
            {
                var id=UnityEngine.PlayerPrefs.GetString(GetStringHashAsHex("_HardwareID"),"");
                if (string.IsNullOrEmpty(id))
                {
                    id = GetStringHashAsHex(Guid.NewGuid().ToString("D") + DateTime.Now.Ticks);
                    UnityEngine.PlayerPrefs.SetString(GetStringHashAsHex("_HardwareID"), id);
                    UnityEngine.PlayerPrefs.Save();
                }

                return GetStringHashAsHex(UnityEngine.SystemInfo.deviceName + UnityEngine.SystemInfo.deviceUniqueIdentifier+id);
            }
        }
        #endregion



        #region GetStringHash
        /// <summary>
        /// String Hash
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        static internal uint GetStringHash(string str)
        {
            System.Text.Encoding encoding = System.Text.Encoding.Unicode;
            System.Security.Cryptography.MD5 md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
            byte[] result = md5.ComputeHash(encoding.GetBytes(str));
            return BitConverter.ToUInt32(result, 0);
        }
        #endregion

        #region GetStringHashAsHex
        internal static string GetStringHashAsHex(string s)
        {
            return string.Format("{0:X}", GetStringHash(s));
        }
        #endregion

    }
    #endregion

    

}
