﻿
using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

/// <summary>
/// SerialNumber_Dialog,
/// for validate use ShowLicenseRequsetAndValidate() function
/// </summary>
public class SerialNumber_Dialog : MonoBehaviour
{
    public delegate void LicenseValidateFunction(bool license_is_valid);
    public delegate void LicenseRquestFunction(string hardwareID, string eMail);
    System.DateTime start_time = System.DateTime.UtcNow;

    public static GUISkin Skin;

    #region ShowLicenseRequsetAndValidate
    /// <summary>
    /// ShowLicenseRequsetAndValidate
    /// </summary>
    /// <param name="caption"></param>
    /// <param name="email_for_request"></param>
    /// <param name="exit_OnCancel"></param>
    /// <param name="ValidateCallback"></param>
    public static void ShowLicenseRequsetAndValidate(string caption
        , string email_for_request
        , bool exit_OnCancel = true
        , bool hide_if_license_valid = true
        , LicenseValidateFunction ValidateCallback = null
        , LicenseRquestFunction CustomSendLicenseRequst = null
        )
    {

        if (updater == null)
        {
            updater = new GameObject("Message dialog");
            updater.AddComponent<SerialNumber_Dialog_Cleanup>();
            updater.hideFlags = HideFlags.HideInHierarchy;

        }

        SerialNumber_Dialog instance = updater.AddComponent<SerialNumber_Dialog>();

        int? license_expired_after_num_days = null;
        var ok_LicenseVerify = DevXUnityTools.SerialNumberValidateTools.Verify(out license_expired_after_num_days);

        instance.ok_LicenseVerify = ok_LicenseVerify;
        instance.license_expired_after_num_days = license_expired_after_num_days;
        instance.ValidateCallback = ValidateCallback;
        instance.CustomSendLicenseRequst = CustomSendLicenseRequst;

        instance.toDestroyOnClose = false; // 
        instance.IsOpened = true;

        instance.Exit_OnCancel = exit_OnCancel;
        instance.Caption = caption;
        instance.email_for_request = email_for_request;

        if (ok_LicenseVerify && hide_if_license_valid)
        {
            instance.IsOpened = false;

            if (ValidateCallback != null)
            {
                ValidateCallback(ok_LicenseVerify);
            }
            return;
        }

    }
    #endregion


    //----------------
    #region field
    bool Exit_OnCancel = true;
    string email_for_request;
    string Caption;
    /// <summary>
    /// 	- The function to be called when the window closes.
    /// </summary>
    public LicenseValidateFunction ValidateCallback;
    public LicenseRquestFunction CustomSendLicenseRequst;
    /// <summary>
    /// 	- Gets or sets a value indicating whether the window is open.
    /// </summary>
    bool IsOpened { get; set; }

    /// <summary>
    /// 	- If set to true, the window will be centered every OnGUI call.
    /// </summary>
    bool center = true;

    /// <summary>
    /// 	- If set to true, this instance will destroy itself when the window closes.
    /// </summary>
    bool toDestroyOnClose = false;

    /// <summary>
    /// 	- The window dimensions.
    /// </summary>
    public Rect windowSize = new Rect(0, 0, 500, 350);

    private static GameObject updater;

    bool ok_LicenseVerify;
    int? license_expired_after_num_days = null;

    string eMail = "";

    #endregion






    #region SerialNumber_Dialog_Cleanup
    /// <summary>
    /// SerialNumber_Dialog_Cleanup
    /// </summary>
    public class SerialNumber_Dialog_Cleanup : MonoBehaviour
    {
        void OnApplicationQuit()
        {
            Destroy(gameObject);
        }

        void OnLevelLoaded()
        {
            Destroy(gameObject);
        }
    }
    #endregion

    #region Start
    void Start()
    {
        InitStyles();
        if (Skin == null)
        {
            Skin = Resources.Load<GUISkin>("LicenseDialogSkin");
        }
     
    }
    #endregion


    #region Update
    void Update()
    {
        DevXUnityTools.SerialNumberValidateTools.UpdateTime();

        if ( (DateTime.UtcNow-start_time).TotalHours>12)
        {
            Application.Quit();
        }
        if ((DateTime.UtcNow - start_time).TotalMinutes < 1f)
            start_time = DateTime.UtcNow;


        if (IsOpened)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Close();
            }
        }
    }
    #endregion


    #region OnGUI
    private void OnGUI()
    {
        if (IsOpened)
        {
            var old_skin = GUI.skin;
            if (Skin != null)
            {
                GUI.skin = Skin;
            }
            GUILayout.BeginArea(new Rect(0, 0, Screen.width, Screen.height), st_screen_fon);

            if (center) windowSize.center = new Vector2(Screen.width * 0.5f, Screen.height * 0.3f);
            if (windowSize.y < 10)
                windowSize.y = 10;


            GUI.Window(0, windowSize, DrawDialogUI, Caption);

            GUILayout.EndArea();

            GUI.skin = old_skin;
        }
    }
    #endregion

    #region OnDestroy
    private void OnDestroy()
    {
        if (ok_LicenseVerify == false)
        {
            if (Exit_OnCancel)
                Application.Quit();
        }

        if (IsOpened && ValidateCallback != null) ValidateCallback(ok_LicenseVerify);
    }

    #endregion

    #region PublicFunctions
    /// <summary>
    /// 	- Opens this instance.
    /// </summary>
    public void Open()
    {
        IsOpened = true;
    }


    /// <summary>
    /// 	- Closes this instance.
    /// </summary>
    public void Close()
    {
        if (IsOpened && ValidateCallback != null) ValidateCallback(ok_LicenseVerify);
        IsOpened = false;

        if (ok_LicenseVerify == false)
        {
            if (Exit_OnCancel)
                Application.Quit();
        }


        if (toDestroyOnClose) Destroy(this);
    }

    /// <summary>
    /// 	- Closes this instance.
    /// </summary>
    public void DialogOK()
    {
        if (IsOpened && ValidateCallback != null)
            ValidateCallback(ok_LicenseVerify);

        IsOpened = false;

        if(ok_LicenseVerify==false)
        {
            if (Exit_OnCancel)
                Application.Quit();
        }

        if (toDestroyOnClose) Destroy(this);
    }
    #endregion

   

    #region InitStyles
    /// <summary>
    /// Style valiables
    /// </summary>
    GUIStyle st_screen_fon;
    GUIStyle st_group;
    GUIStyle st_group_red;
    GUIStyle st_group_green;

    /// <summary>
    /// Initializing styles
    /// </summary>
    void InitStyles()
    {
        // Load end decript images

        st_group = new GUIStyle();
        var texture = new Texture2D(1, 1);
        texture.SetPixel(0, 0, new Color(0f, 0f, 0f, 0.3f));
        texture.Apply();
        st_group.normal.background = texture;

        st_screen_fon = new GUIStyle();
        texture = new Texture2D(1, 1);
        texture.SetPixel(0, 0, new Color(0.7f, 0.8f, 0.9f, 0.8f));
        texture.Apply();
        st_screen_fon.normal.background = texture;


        st_group_red = new GUIStyle();
        texture = new Texture2D(1, 1);
        texture.SetPixel(0, 0, new Color(1f, 0f, 0f, 0.3f));
        texture.Apply();
        st_group_red.normal.background = texture;

        st_group_green = new GUIStyle();
        texture = new Texture2D(1, 1);
        texture.SetPixel(0, 0, new Color(0f, 1f, 0f, 0.3f));
        texture.Apply();
        st_group_green.normal.background = texture;
    }
    #endregion


    #region DrawDialogUI
    private void DrawDialogUI(int ID)
    {
        
        GUILayout.BeginVertical();//windowStyle

        GUILayout.Label("Current date: " + System.DateTime.Now.ToString("yyyy.MM.dd"));

        GUILayout.Label("HardwareID"); GUILayout.TextField(DevXUnityTools.SerialNumberValidateTools.HardwareID);

        GUILayout.Label("eMail"); eMail = GUILayout.TextField(eMail);

        GUILayout.Space(10);


        if (GUILayout.Button("Send license email request"))
        {

            GUI.FocusControl("");

            if (CustomSendLicenseRequst == null)
            {
                //Application.OpenURL()

                //
                // Show email client for license request
                //
                //System.Diagnostics.Process.Start(("mailto:" + email_for_request + "?subject=" + "Serial number request for "+Application.productName
                //    + "&body="
                //    + "ProductName=" + Application.productName + "%0A"
                //    + "HardwareID=" + DevXUnityTools.SerialNumberValidateTools.HardwareID + "%0A"
                //    + "Client eMail=" + eMail + "%0A"
                //    + "DateTime=" + System.DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss (K)")+ "%0A"
                //    )
                //);
                Application.OpenURL(("mailto:" + email_for_request + "?subject=" + "Serial number request for " + Application.productName
                    + "&body="
                    + "ProductName=" + Application.productName + "%0A"
                    + "HardwareID=" + DevXUnityTools.SerialNumberValidateTools.HardwareID + "%0A"
                    + "Client eMail=" + eMail + "%0A"
                    + "DateTime=" + System.DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss (K)") + "%0A"
                    )
                );

            }
            else
            {
                CustomSendLicenseRequst(DevXUnityTools.SerialNumberValidateTools.HardwareID, eMail);
            }
        }



        GUILayout.Label("Enter serial number key:");
        string res = GUILayout.TextArea(DevXUnityTools.SerialNumberValidateTools.SerialNumberKey);
        if (res != DevXUnityTools.SerialNumberValidateTools.SerialNumberKey)
        {
            //
            // Save new user serial number key
            //
            DevXUnityTools.SerialNumberValidateTools.SerialNumberKey = res;


            // Verify new user license key
            ok_LicenseVerify = DevXUnityTools.SerialNumberValidateTools.Verify(out license_expired_after_num_days);
        }

        GUILayout.Space(10);


        GUILayout.BeginVertical(
            ok_LicenseVerify ?
                st_group_green : // valid
                st_group_red,   // not valid
            GUILayout.Width(windowSize.width));
        {
            GUILayout.Space(6);

            var old1 = GUI.backgroundColor;
            if (ok_LicenseVerify)
            {
                GUILayout.Label("License - Valid"+(license_expired_after_num_days.HasValue?" (expired after "+ license_expired_after_num_days.Value+ " days)":""));
            }
            else
            {
                GUILayout.Label("License - NOT VALID!" + (license_expired_after_num_days.HasValue ? " (expired by time)" : ""));
            }
            GUI.backgroundColor = old1;
        }
        GUILayout.EndVertical();

        GUILayout.Space(20);


        GUILayout.BeginHorizontal();
        {
            GUILayout.Label("", GUILayout.Width(windowSize.width - 250));
            var old = GUI.enabled;
            GUI.enabled = ok_LicenseVerify;
            if (GUILayout.Button("OK", GUILayout.Width(100)))
            {
                DialogOK();
            }
            GUI.enabled= old;

            if (GUILayout.Button("Close", GUILayout.Width(100)))
            {
                Close();
            }
        }
        GUILayout.EndHorizontal();


        GUILayout.EndVertical();
    }

    #endregion

}

