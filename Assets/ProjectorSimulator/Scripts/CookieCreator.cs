﻿using UnityEngine;
using System.Linq;

public struct CookieData
{
    public float shift_v, shift_h, ratio, aspect;
    public CookieData(float shiftV, float shiftH, float throwRatio, float imageAspect)
    {
        shift_v = shiftV;
        shift_h = shiftH;
        ratio = throwRatio;
        aspect = imageAspect;
    }    
}

public class Cookie
{
    public enum ImageType { Colour, Grey};
    Texture2D projectedImage = null;
    Color32[] imageColours = null;
    CookieData data;
    float imageWidth, imageHeight;
    float maxImageEdgeDistance;
    float metersToPixels = 1.0f;
    ImageType imageType = ImageType.Grey;
    Texture2D redCookie, greenCookie, blueCookie;
    Color32[] redColors, greenColors, blueColors;

    // used for calculating angle
    const float distance = 10.0f; // throw distance
    int textureSize = 1024; // texture width and height

    Light redLight, greenLight, blueLight;

    public Cookie(CookieData cookieData, int cookieSize, Light red, Light green, Light blue, Texture2D imageToProject = null, bool colour = false)
    {
        textureSize = cookieSize;
        projectedImage = imageToProject;
        if (projectedImage != null)
            imageColours = projectedImage.GetPixels32();
        else
            imageColours = null;

        CreateTexture();

        if (colour)
            imageType = ImageType.Colour;

        redLight = red;
        greenLight = green;
        blueLight = blue;

        data = cookieData;

        Initialise();
    }

    /// <summary>
    /// Creates the Cookie textures and gets the pixel buffers of each
    /// </summary>
    void CreateTexture()
    {
        redCookie   = new Texture2D(textureSize, textureSize, TextureFormat.Alpha8, false);
        greenCookie = new Texture2D(textureSize, textureSize, TextureFormat.Alpha8, false);
        blueCookie  = new Texture2D(textureSize, textureSize, TextureFormat.Alpha8, false);
        redCookie.wrapMode = greenCookie.wrapMode = blueCookie.wrapMode = TextureWrapMode.Clamp;

        redColors   = redCookie.GetPixels32();
        greenColors = greenCookie.GetPixels32();
        blueColors  = blueCookie.GetPixels32();
    }

    /// <summary>
    /// Calculates light cone angle, draws the first Cookie, assigns cookie to light.
    /// TODO: assign cookies to lights in ProjectorSim class - will allow light cookies to be switched between CookieCreators, for slideshow of several images
    /// </summary>
    void Initialise()
    {
        // calculate angle of light cone from throw ratio and possible lens shift amount
        imageWidth = distance / data.ratio;
        imageHeight = imageWidth / data.aspect;

        // calculate shift **IN METERS**
        float shift_H = imageWidth * (data.shift_h / 200.0f);
        float shift_V = imageHeight * (data.shift_v / 200.0f);

        // calculate how far the image can move with full lens shift applied (in meters from lens centre)
        float imageLimit_h = (imageWidth / 2.0f) + Mathf.Abs(shift_H);
        float imageLimit_v = (imageHeight / 2.0f) + Mathf.Abs(shift_V);
        maxImageEdgeDistance = Mathf.Max(imageLimit_h, imageLimit_v);

        metersToPixels = textureSize / (maxImageEdgeDistance * 2f);

        // Calculate the light angle required
        float spotAngle = Mathf.Atan(maxImageEdgeDistance / distance) * 2 * Mathf.Rad2Deg;
        redLight.spotAngle = greenLight.spotAngle = blueLight.spotAngle = spotAngle;

        // draw the cookie(s)
        UpdateCookie();
    }

    /// <summary>
    /// Called when image size/pos is changed and the image shape in the cookie will change
    /// </summary>
    /// <param name="cookieData"></param>
    public void Reinitialise(CookieData cookieData)
    {
        data = cookieData;
        Initialise();
    }

    /// <summary>
    /// Calculates where the image will be in the cookie and creates the whole cookie texture
    /// </summary>
    void UpdateCookie()
    {
        float resultWidth, resultHeight;
        resultWidth = distance / data.ratio;
        resultHeight = resultWidth / data.aspect;

        // calculate image position in pixels

        // calculate shift in meters
        Vector2 shift = new Vector2(resultWidth  * (data.shift_h / 200.0f),
                                    resultHeight * (data.shift_v / 200.0f) );

        // position of image in meters, relative to projector centre
        float imageLeftMeters = maxImageEdgeDistance - (resultWidth  / 2.0f) + shift.x;
        float imageTopMeters  = maxImageEdgeDistance - (resultHeight / 2.0f) + shift.y;

        // posisiton of image in the cookie texture
        int imageLeftPixels = (int)(imageLeftMeters * metersToPixels);
        int imageTopPixels  = (int)(imageTopMeters  * metersToPixels);

        // size of image in the cookie texture
        int imageWidthPixels  = (int)(resultWidth * metersToPixels);
        int imageHeightPixels = (int)(resultHeight * metersToPixels);

        int x, y, pi_x, pi_y;
        float f;

        byte red, green, blue;
        red = green = blue = 255;

        //float s = Time.realtimeSinceStartup;
        //float ds = 0f;

        // set pixel colors //TODO: optimise - can set only pixels that are changing?
        for (int i = 0; i < redColors.Length; i++)
        {
            x = i % textureSize;
            y = i / textureSize;

            // reached image start row?
            if (y > imageTopPixels &&
                // reached image start column?
                x > imageLeftPixels &&
                // before last column?
                x < imageLeftPixels + imageWidthPixels && x < textureSize - 1 &&
                // before last row?
                y < imageTopPixels + imageHeightPixels && y < textureSize - 1)
            {
                // white if no projected image
                if (projectedImage == null)
                {
                    redColors[i]   = new Color32(255, 255, 255, 255);

                    // Also set green and blue channels otherwise we get artefacts if colour box is checked
                    if (imageType == ImageType.Colour)
                    {
                        greenColors[i] = new Color32(255, 255, 255, 255);
                        blueColors[i] = new Color32(255, 255, 255, 255);
                    }
                }
                else
                {
                    // select which pixel to take from the image
                    pi_x = Mathf.RoundToInt(Mathf.Lerp(0, projectedImage.width - 1, (x - imageLeftPixels) / (float)imageWidthPixels));
                    pi_y = Mathf.RoundToInt(Mathf.Lerp(0, projectedImage.height - 1, (y - imageTopPixels) / (float)imageHeightPixels));
                    int flatindex = (projectedImage.width * pi_y) + pi_x;

                    if (flatindex > imageColours.Length)
                        Debug.Log("imageColours length = " + imageColours.Length + ", flatindex = " + flatindex);

                    // Colour or greyscale?
                    switch (imageType)
                    {
                        case ImageType.Colour:
                            red = (byte)Mathf.Clamp((imageColours[flatindex].r), 0, 255);
                            redColors[i] = new Color32(255, 255, 255, red);
                            green = (byte)Mathf.Clamp((imageColours[flatindex].g), 0, 255);
                            greenColors[i] = new Color32(255, 255, 255, green);
                            blue = (byte)Mathf.Clamp((imageColours[flatindex].b), 0, 255);
                            blueColors[i] = new Color32(255, 255, 255, blue);
                            break;
                        default: // greyscale
                            f = ((float)imageColours[flatindex].r +
                                        imageColours[flatindex].g +
                                        imageColours[flatindex].b) / 3f;
                            red = (byte)Mathf.Clamp((int)f, 0, 255);
                            redColors[i] = new Color32(255, 255, 255, red);
                            break;
                    }
                }
            }
            else // BLACK PIXELS
            {
                redColors[i] = new Color32(255, 255, 255, 0);
                if (imageType == ImageType.Colour)
                {
                    greenColors[i] = new Color32(255, 255, 255, 0);
                    blueColors[i]  = new Color32(255, 255, 255, 0);
                }
            }
        }

        // apply new colours
        redCookie.SetPixels32(redColors);
        redCookie.Apply();
        if (imageType == ImageType.Colour)
        {
            greenCookie.SetPixels32(greenColors);
            greenCookie.Apply();
            blueCookie.SetPixels32(blueColors);
            blueCookie.Apply();
        }

        //ds = Time.realtimeSinceStartup - s;
        //Debug.Log("Time taken (ms): " + (ds * 1000f).ToString());
    }


    /// <summary>
    /// Set the image to project (does NOT cause a redraw - call Reinitialise to redraw)
    /// </summary>
    /// <param name="image"></param>
    /// <param name="pixels"></param>
    public void SetProjectedImage(Texture2D image)
    {
        projectedImage = image;
        if (image != null)
            imageColours = image.GetPixels32();
    }
    public void RemoveProjectedImage() { projectedImage = null; imageColours = null; }
    public void SetProjectedImageType(ImageType type)
    {
        imageType = type;
        if (imageType == ImageType.Colour) // colour mode?
            redLight.color = Color.red;
        else
            redLight.color = Color.white;
        greenLight.enabled = blueLight.enabled = imageType == ImageType.Colour;
    }
    public void SetCookieSize(int newSize)
    {
        textureSize = newSize;
        CreateTexture();
        Initialise();
    }

    public Texture2D GetRedCookie()   { return redCookie;   }
    public Texture2D GetGreenCookie() { return greenCookie; }
    public Texture2D GetBlueCookie()  { return blueCookie;  }
}
