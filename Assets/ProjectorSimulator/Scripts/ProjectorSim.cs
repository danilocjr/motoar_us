﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System.Collections;
using System.Collections.Generic;


[ExecuteInEditMode]
public class ProjectorSim : MonoBehaviour
{
    
    public enum CookieSizes { c_128, c_256, c_512, c_1024, c_2048, c_4096};
    
    public bool live = false;
    [Tooltip("Aspect ratio of the projected image (width/height)")]
    public float aspectRatio = 1.6f;
    float lastAspect = 1.6f;
    [Tooltip("Throw ratio of the projector (distance/image width) - smaller is wider")]
    public float throwRatio = 1.0f;
    float lastThrowRatio = 1.0f;
    [Tooltip("Vertical lens shift as a percentage of image width/2 (100% shift = lens level with image edge)")]
    public float shift_v = 0.0f;
    float lastShiftV = 0.0f;
    [Tooltip("Horizontal lens shift as a percentage of image width/2 (100% shift = lens level with image edge)")]
    public float shift_h = 0.0f;
    float lastShiftH = 0.0f;
    [Tooltip("Allows control of the brightness of the projector")]
    [Range(0.0f, 8.0f)]
    public float brightness = 1.0f;
    float lastBrightness = 1.0f;
    [Tooltip("Image to project. If empty, only white will be projected.")]
    public List<Texture2D> images = null;
    Texture2D lastImage = null;
    [Tooltip("zero-based index of the image to preview in the editor")]
    public int previewImageIndex = 0;
    int lastPreviewImageIndex = 0;
    [Tooltip("The time to show each image, if more than one image is supplied. Otherwise this value is ignored.")]
    public float imageInterval = 5.0f;
    [Tooltip("Whether to loop the slideshow. If false, the last image will remain once the slideshow is complete.")]
    public bool loop = true;
    [Tooltip("Whether to play the slideshow immediately.")]
    public bool playOnAwake = true;
    [Tooltip("Only used with an image. If unchecked, image will be in greyscale (loads slightly faster). Recommended to leave greyscale while setting aspect, zoom, and shift.")]
    public bool colour = false;
    bool lastColour = false;
    [Tooltip("The resolution of the Cookie texture. Higher will have better clarity in the image, lower will be faster to generate. Recommended to keep this at 256 untl the image is positioned.")]
    public CookieSizes cookieSize = CookieSizes.c_256;
    CookieSizes lastCookieSize = CookieSizes.c_256;

    // Array of Cookies for slideshow
    Cookie[] cookies;
    int slideshowIndex = 0; // start slideshow at first image
    Light[] lights;

    // Use this for initialization
    void Awake ()
    {
        lights = GetComponentsInChildren<Light>();
        foreach (Light l in lights)
        {
            l.enabled = false;
        }

        // ensure there is always at least 1 entry in the array
        if (images.Count > 0)
            cookies = new Cookie[images.Capacity];
        else
            cookies = new Cookie[1]; // no images but we need a cookie for white

        int cookieSizeInt = int.Parse(cookieSize.ToString().Substring(2));

#if UNITY_EDITOR
        // if in editor edit mode, ensure preview image index is valid
        if (!EditorApplication.isPlaying)
        {
            previewImageIndex = Mathf.Clamp(previewImageIndex, 0, images.Count - 1); //  don't allow user to choose a value out of range
        }
        
        // if in editor play mode, calculate all cookies (because we want to play the slideshow)
        if (EditorApplication.isPlaying)
        {
#endif
            // if in any play mode, generate all cookies for slideshow
            for (int i = 0; i < cookies.Length; i++)
            {
                cookies[i] = new Cookie(new CookieData(shift_v, shift_h, throwRatio, aspectRatio), cookieSizeInt, lights[0], lights[1], lights[2], images[i], colour);
            }
#if UNITY_EDITOR
        }
        else // EDITOR EDIT MODE - only calculate the preview cookie
        {
            // use slot 0 as the preview cookie
            cookies[0] = new Cookie(new CookieData(shift_v, shift_h, throwRatio, aspectRatio), cookieSizeInt, lights[0], lights[1], lights[2], images[previewImageIndex], colour);
        }
#endif
        AssignLightCookies();
    }

    // When enabled, turn lights on. Also start slideshow if necessary.
    void OnEnable()
    {
        lights[0].enabled = true;
        if (colour)
            lights[1].enabled = lights[2].enabled = true;

// allow slideshow in editor play mode
#if UNITY_EDITOR
        if (EditorApplication.isPlaying)
        {
#endif
            // queue the next frame, if there is one.
            if (playOnAwake && cookies.Length > 1 && slideshowIndex < cookies.Length && imageInterval > 0.0f)
            {
                Invoke("AdvanceSlideshow", imageInterval);
            }
#if UNITY_EDITOR
        }
#endif
    }

    // When disabled, turn lights off
    void OnDisable()
    {
        foreach (Light l in lights)
        {
            l.enabled = false;
        }
        CancelInvoke("AdvanceSlideshow");
    }
    /// <summary>
    /// Allow external scripts to set the slideshow index
    /// </summary>
    /// <param name="newIndex"></param>
    public void SetSlideshowIndex(int newIndex)
    {
        if (newIndex >= 0 && newIndex < images.Count)
        {
            slideshowIndex = newIndex;
        }
        else
        {
            Debug.LogWarning("Slideshow index " + newIndex + " is out of range. Argument must be between 0 and " + (images.Count - 1));
            return;
        }

        AssignLightCookies();

        // restart the slideshow timer
        CancelInvoke("AdvanceSlideshow");
        Invoke("AdvanceSlideshow", imageInterval);
    }

    // Allow external scripts to pause and play the slideshow
    public void PauseSlideshow()
    {
        playOnAwake = false;
        CancelInvoke("AdvanceSlideshow");

    }
    public void PlaySlideshow()
    {
        CancelInvoke("AdvanceSlideshow");
        playOnAwake = true;
        if (images.Count >= 1 && imageInterval > 0.0f)
            Invoke("AdvanceSlideshow", imageInterval);
    }

    /// <summary>
    /// Gives each light a cookie for its relevant channel, using the slideshowIndex value.
    /// </summary>
    void AssignLightCookies()
    {
        lights[0].cookie = cookies[slideshowIndex].GetRedCookie();
        lights[1].cookie = cookies[slideshowIndex].GetGreenCookie();
        lights[2].cookie = cookies[slideshowIndex].GetBlueCookie();
    }

    public void AdvanceSlideshow()
    {
        // cancel any more pending invokes - otherwise external use of this function can cause inconsistent frame rate
        CancelInvoke("AdvanceSlideshow");

        slideshowIndex++;
        if (slideshowIndex >= cookies.Length) // reached end?
        {
            if (loop)
                slideshowIndex = 0;
            else
                return; // should never normally be hit, unless AdvanceSlideshow is called externally
        }

        AssignLightCookies();

        bool moreimages = slideshowIndex < cookies.Length - 1 || (slideshowIndex >= cookies.Length - 1 && loop);

        // if there is another frame, queue it.
        if (moreimages)
            Invoke("AdvanceSlideshow", imageInterval);
    }

#if UNITY_EDITOR
    void UpdateColour()
    {
        if (!EditorApplication.isPlaying)
        {
            if (colour)
                cookies[0].SetProjectedImageType(Cookie.ImageType.Colour);
            else
                cookies[0].SetProjectedImageType(Cookie.ImageType.Grey);
        }
    }
#endif

#if UNITY_EDITOR
    void OnValidate()
    {
        if (images.Count == 0) // ensure there is always at least 1 slot in the images array
        {
            images.Add(null);
        }
        if (cookies != null && cookies.Length > 0)
        {
            previewImageIndex = Mathf.Clamp(previewImageIndex, 0, images.Count - 1); // ensure PreviewImageIndex is valid

            if (live && cookies[slideshowIndex] != null)
            {
                // preview image has changed?
                if (images[previewImageIndex] != lastImage || previewImageIndex != lastPreviewImageIndex)
                {
                    if (images[previewImageIndex])
                    {
                        cookies[0].SetProjectedImage(images[previewImageIndex]);
                    }
                    else
                    {
                        cookies[0].RemoveProjectedImage();
                    }
                }
                if (colour != lastColour)
                {
                    UpdateColour();
                }
                // recalculate cookie
                if (aspectRatio != lastAspect ||
                    throwRatio != lastThrowRatio ||
                    shift_h != lastShiftH ||
                    shift_v != lastShiftV ||
                    colour != lastColour ||
                    previewImageIndex != lastPreviewImageIndex ||
                    images[previewImageIndex] != lastImage)
                {
                    cookies[0].Reinitialise(new CookieData(shift_v, shift_h, throwRatio, aspectRatio));

                    lastColour = colour;
                    lastAspect = aspectRatio;
                    lastThrowRatio = throwRatio;
                    lastShiftH = shift_h;
                    lastShiftV = shift_v;
                    lastPreviewImageIndex = previewImageIndex;
                    lastImage = images[previewImageIndex];
                    AssignLightCookies();
                }
                if (cookieSize != lastCookieSize)
                {
                    cookies[0].SetCookieSize(int.Parse(cookieSize.ToString().Substring(2)));
                    lastCookieSize = cookieSize;
                    AssignLightCookies();
                }
                if (lastBrightness != brightness)
                {
                    foreach (Light l in lights)
                    {
                        l.intensity = brightness;
                        lastBrightness = brightness;
                    }
                }
            }
        }
    }
#endif
}
