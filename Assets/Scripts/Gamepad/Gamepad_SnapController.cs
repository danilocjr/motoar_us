﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Vuforia
{
    public class Gamepad_SnapController : MonoBehaviour, ITrackableEventHandler
    {
        private TrackableBehaviour mTrackableBehaviour;

        // Commons
        public string resFolder;
      
        //Reality Controllers
        public RealityComponentsManager _vr;
        public RealityComponentsManager _ar;

        public ModelController _vrModel;

        // UI elements
        public WingSpriteController _wings;
        public UIManager _ui;

        public GameObject _game;

        public PlayVideo _background;

        // Private Area
        public Vector3 _targetPosition;
        public Vector3 _targetRotation;

        private string _lastMode = "";
        private string _lastReality = "AR";

        public bool InvertControls = false;
        /// 
        /// Monobehaviour Methods
        /// 
        void Start()
        {
            mTrackableBehaviour = GetComponent<TrackableBehaviour>();
            if (mTrackableBehaviour)
            {
                mTrackableBehaviour.RegisterTrackableEventHandler(this);
            }

            _game.SetActive(false);
        }

        void Update()
        {
            if (mTrackableBehaviour.CurrentStatus == TrackableBehaviour.Status.TRACKED)
            {
                _targetRotation = this.transform.localRotation.eulerAngles;

                _ar.UpdatePositionRotation(this.transform.position, _targetRotation);
                _vr.UpdatePositionRotation(this.transform.position, _targetRotation);

                // landscape MODE:
                if (_targetRotation.x > 350.0f && _targetRotation.z > 120f)
                {
                    _wings.EnableAt();

                    if (_lastMode != "landscape")
                    {
                        _lastMode = "landscape";
                        OnModeChangeToLandscape();
                    }
                    else
                    {
                        OnModeLandscape();
                    }

                }
                // portrait MODE:
                else if (_targetRotation.x > 250.0f && _targetRotation.x < 290.0f)
                {
                    _wings.EnableAt(_targetPosition.x);

                    if (_lastMode != "portrait")
                    {
                        _lastMode = "portrait";
                        OnModeChangeToPortrait();
                    }
                    else
                    {
                        OnModePortrait();
                    }
                }
            }
        }

        /// 
        /// Especial and Custom Methods 
        /// 
        /// 
       
        void RunReality(string realityMode)
        {
            if (realityMode == "AR")
            {
                _lastReality = "AR";

                _background.Stop();

                _game.SetActive(false);
               
                _ar.On();
                _vr.Off();
                _ui.GetComponent<Canvas>().worldCamera = _ar.realityCamera;

                _ui.SetInfo("turn");
            }
            else
            {
                _lastReality = "VR";

                _background.Play();

                _game.SetActive(true);

                _vr.On();
                _ar.Off();
                _ui.GetComponent<Canvas>().worldCamera = _vr.realityCamera;

                _ui.SetInfo("move");

            }
        }

        void StopAllReality(){

            _background.Stop();

            _game.SetActive(false);
            _ar.Off(true);
            _vr.Off();
            _ui.GetComponent<Canvas>().worldCamera = _ar.realityCamera;
        }
     
        /// 
        /// Orientation and Commom Elements Methods 
        /// 
        void OnModeChangeToPortrait()
        { 
            RunReality("AR");
        }
        void OnModePortrait()
        {
            
        }
        void OnModeChangeToLandscape()
        {
            /*
            if (_lastReality == "AR")
            {
                if (_targetRotation.z < 120f)
                {
                    InvertControls = false;
                    _vrModel.invert = false;
                }
                else
                {
                    InvertControls = true;
                    _vrModel.invert = true; 
                }
            }
            */
            RunReality("VR");
        }
        void OnModeLandscape()
        {
            //DO SOMETHING
        }


        /// 
        /// Vuforia and Itrackable Methods
        /// 
        public void OnTrackableStateChanged(
                                        TrackableBehaviour.Status previousStatus,
                                        TrackableBehaviour.Status newStatus)
        {
            if (newStatus == TrackableBehaviour.Status.DETECTED ||
                newStatus == TrackableBehaviour.Status.TRACKED ||
                newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
            {
                OnTrackingFound();
            }
            else
            {
                OnTrackingLost();
            }
        }

        private void OnTrackingFound()
        {
            _ui.SetInfo("turn");
          
            Renderer[] components = GetComponentsInChildren<Renderer>();
            foreach (Renderer component in components)
            {
                component.enabled = true;
            }
            SpriteRenderer[] scomponents = GetComponentsInChildren<SpriteRenderer>();
            foreach (SpriteRenderer scomponent in scomponents)
            {
                scomponent.enabled = true;
            }

            RunReality(_lastReality);
        }

        private void OnTrackingLost()
        {
            StopAllReality();

            _ui.SetInfo();
            _wings.EnableAt();
           

            Renderer[] components = GetComponentsInChildren<Renderer>();
            foreach (Renderer component in components)
            {
                component.enabled = false;
            }
            SpriteRenderer[] scomponents = GetComponentsInChildren<SpriteRenderer>();
            foreach (SpriteRenderer scomponent in scomponents)
            {
                scomponent.enabled = false;
            }
        }
    }
}
