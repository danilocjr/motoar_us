﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayVideo : MonoBehaviour {

    public SpriteRenderer _mr;
    private AudioSource _as;

    private void Start()
    {
        _as = GetComponent<AudioSource>();
        _mr.enabled = false;
    }

    public void Play()
    {
        _as.Play();
        _mr.enabled = true;
    }

    public void Stop()
    {
        _as.Stop();
        _mr.enabled = false;
    }

}
