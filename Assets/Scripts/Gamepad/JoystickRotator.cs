﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JoystickRotator : MonoBehaviour {

    public RealityComponentsManager _parent;
    public bool invx = true, invy = false, invz = false;

	// Update is called once per frame
	void Update () {

        transform.rotation = Quaternion.Euler(_parent.GetRotation().x * (invx ? 1 : -1),
                                              _parent.GetRotation().y * (invy ? 1 : -1),
                                              _parent.GetRotation().z * (invz ? 1 : -1));


        transform.rotation *= Quaternion.Euler(-90f, 0f, 0f);

	}
}
