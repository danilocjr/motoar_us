﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ARProjectorController : MonoBehaviour {


    public void On()
    {
        this.GetComponentInChildren<Light>().enabled = true;
        
        Renderer[] rcomponents = this.GetComponentsInChildren<Renderer>(true);
        foreach (Renderer component in rcomponents)
        {
            component.enabled = true;
        }
        SpriteRenderer[] scomponents = this.GetComponentsInChildren<SpriteRenderer>(true);
        foreach (SpriteRenderer component in scomponents)
        {
            component.enabled = true;
        }
    }

    public void Off()
    {
        this.GetComponentInChildren<Light>().enabled = false;

        Renderer[] rcomponents = this.GetComponentsInChildren<Renderer>(true);
        foreach (Renderer component in rcomponents)
        {
            component.enabled = false;
        }
        SpriteRenderer[] scomponents = this.GetComponentsInChildren<SpriteRenderer>(true);
        foreach (SpriteRenderer component in scomponents)
        {
            component.enabled = false;
        }
    }
}
