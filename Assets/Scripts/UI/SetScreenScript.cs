﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetScreenScript : MonoBehaviour {

    public SpriteRenderer startScreen;
    public SpriteRenderer idleScreen;
    public SpriteRenderer experimente;

    public bool StartScreenIsShowing = false;
   
    public void SetScreen(string mode){

        switch (mode)
        {
            case "start":
                StartScreenIsShowing = true;
                startScreen.enabled = true;
                idleScreen.enabled = false;
                experimente.enabled = false;
                break;
            case "idle":
                StartScreenIsShowing = false;
                startScreen.enabled = false;
                idleScreen.enabled = true;
                experimente.enabled = false;
                break;
            case "try":
                StartScreenIsShowing = false;
                startScreen.enabled = false;
                idleScreen.enabled = false;
                experimente.enabled = true;
                break;
            case "":
            default:
                StartScreenIsShowing = false;
                startScreen.enabled = false;
                idleScreen.enabled = false;
                experimente.enabled = false;
                break;
        }

    }
}
