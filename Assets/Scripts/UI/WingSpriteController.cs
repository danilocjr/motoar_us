﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WingSpriteController : MonoBehaviour {

    public SpriteRenderer wingLeft;
    public SpriteRenderer wingRight;

    public float ChangeTriggerOnScreen = 0.3f;
    public bool isShowing = false;

    public void EnableAt(float xpos)
    {
        isShowing = true;

        if (xpos < ChangeTriggerOnScreen)
        {
            wingLeft.color = new Color(255f, 255f, 255f, 0f);
            wingRight.color = new Color(255f, 255f, 255f, 1f);
        }
        else
        {
            wingLeft.color = new Color(255f, 255f, 255f, 1f);
            wingRight.color = new Color(255f, 255f, 255f, 0f);
        }
    }

    public void EnableAt()
    {
        isShowing = false;

        wingLeft.color = new Color(255f, 255f, 255f, 0f);
        wingRight.color = new Color(255f, 255f, 255f, 0f);

    }
}
