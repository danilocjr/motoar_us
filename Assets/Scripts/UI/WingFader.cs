﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WingFader : MonoBehaviour
{
    public void Appear()
    {
        SpriteRenderer[] _sprs = GetComponentsInChildren<SpriteRenderer>();
        foreach (SpriteRenderer _spr in _sprs)
        {
            _spr.color = new Color(_spr.color.r, _spr.color.g, _spr.color.b, 1f);
        }
    }

    public void Disappear()
    {
        SpriteRenderer[] _sprs = GetComponentsInChildren<SpriteRenderer>();
        foreach (SpriteRenderer _spr in _sprs)
        {
            _spr.color = new Color(_spr.color.r, _spr.color.g, _spr.color.b, 0f);
        }
    }
}
