﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CornerInfoController : MonoBehaviour {

    public GameObject turn;
    public GameObject rotate;
    public GameObject put;
    public GameObject move;
    public GameObject reach;

    public void SetInfo(string info){
        switch (info){
            case "turn":
                turn.SetActive(true);
                rotate.SetActive(false);
                put.SetActive(false);
                move.SetActive(false);
                reach.SetActive(false);
                break;
            case "rotate":
                turn.SetActive(false);
                rotate.SetActive(true);
                put.SetActive(false);
                move.SetActive(false);
                reach.SetActive(false);             
                break;
            case "put":
                turn.SetActive(false);
                rotate.SetActive(false);
                put.SetActive(true);
                move.SetActive(false);
                reach.SetActive(false);
                break;
            case "move":
                turn.SetActive(false);
                rotate.SetActive(false);
                put.SetActive(false);
                move.SetActive(true);
                reach.SetActive(false);
                break;
            case "reach":
                turn.SetActive(false);
                rotate.SetActive(false);
                put.SetActive(false);
                move.SetActive(false);
                reach.SetActive(true);
                break;
            case "":
            default:
                turn.SetActive(false);
                rotate.SetActive(false);
                put.SetActive(false);
                move.SetActive(false);
                reach.SetActive(false);  
                break;
        }
    }

    public void SetInfo(){
        SetInfo("");
    }
	
}
