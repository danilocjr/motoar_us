﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public CornerInfoController _cornerInfo;

    public SetScreenScript _screen;
    public bool StartScreenIsShowing { get; private set;}

    public UICamera360Script _camera;
    public PowerpackAnimScript _powerpack;

    private void Awake()
    {
        SetInfo();
        SetScreen();
        Cam360Mode();
    }

    public void SetInfo(string info)
    {
        if(!StartScreenIsShowing)
            _cornerInfo.SetInfo(info);
    }

    public void SetInfo()
    {
        _cornerInfo.SetInfo();
    }

    public void SetScreen(string mode)
    {
        _cornerInfo.SetInfo();
        _screen.SetScreen(mode);
        StartScreenIsShowing = _screen.StartScreenIsShowing;
    }

    public void SetScreen()
    {
        SetScreen("");
    }

    public void Cam360Mode(string mode)
    {
        switch(mode){
            case "VR":
                _camera.Countdown(true);
                _camera.FakeCam(true);
                _camera.SnapMask(false);
                break;
            case "AR":
                _camera.Countdown(false);
                _camera.FakeCam(false);
                _camera.SnapMask(true);
                break;
            case "":
            default:
                _camera.Countdown(false);
                _camera.FakeCam(false);
                _camera.SnapMask(false);
                break;
        }
    }
    public void Cam360Mode()
    {
        Cam360Mode("");
    }
    public bool Cam360DoneVR()
    {
        return _camera.IsCountdownDone();
    }

    public void PowerpackAnim(bool status)
    {
        _powerpack.ShowAnimation(status);
    }

    public void PowerPackShowMarker(bool status)
    {
        _powerpack.EnableMask(status);
    }

    public bool IsPowerpackAnimEnd()
    {
        return _powerpack.isAnimEnd;
    }
  
}