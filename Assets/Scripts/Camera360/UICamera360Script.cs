﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UICamera360Script : MonoBehaviour {

    public SpriteRenderer _halfcam;
    public SpriteRenderer _snap;
    public CountdownC360Script _countdown;

    private void Start(){
        _snap.enabled = false;
    }

    public bool IsCountdownDone(){
        return _countdown.done;
    }

    public void FakeCam(bool status){
        _halfcam.enabled = status;
    }

    public void Countdown(bool status){
        _countdown.Counting(status);       
    }

    public void SnapMask(bool status){
        _snap.enabled = status;
    }

}
