﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Vuforia
{
    public class Camera360_SnapController : MonoBehaviour, ITrackableEventHandler
    {
        private TrackableBehaviour mTrackableBehaviour;

        public string resFolder;

        public RealityComponentsManager _ar;
        public RealityComponentsManager _vr;

        public CubeBehaviour _cube;

        public WingSpriteController _wings;

        public UIManager _ui;
       
        private string _lastMode = "";
        private string _lastReality = "AR";

        private int _journeyId = 0;
        private bool _journeyJustEnd = true;
        private bool _lostReached = false;
  
        /// 
        /// Monobehaviour Methods
        /// 
        void Start()
        {
            mTrackableBehaviour = GetComponent<TrackableBehaviour>();
            if (mTrackableBehaviour)
            {
                mTrackableBehaviour.RegisterTrackableEventHandler(this);
            }
        }

        void Update()
        {
            if (mTrackableBehaviour.CurrentStatus == TrackableBehaviour.Status.TRACKED)
            {
                _ar.UpdatePositionRotation(transform.position, transform.localRotation.eulerAngles);
                _vr.UpdatePositionRotation(transform.position, transform.localRotation.eulerAngles);

                if (_lastReality == "AR")
                {
                    OnModePortrait();
                }
                else
                {
                    if (_ui.Cam360DoneVR())
                    {
                        _journeyJustEnd = true;
                        _cube.NextCubemap();

                        RunReality("AR");
                    }
                }

                if (_ui.StartScreenIsShowing)
                {
                    _journeyJustEnd = true;
                }
            }
        }

        void OnModePortrait()
        {
            _wings.EnableAt(_ar.GetPosition().x);

            if ((_ar.GetPosition().x > 0.75f) && (_journeyJustEnd) && (_lastReality=="AR"))
            {
                RunReality("VR");
            }
        }

        /// 
        /// Especial and Custom Methods 
        /// 
        void RunReality(string realityMode)
        {
            if (realityMode == "AR")
            {
                _lastReality = "AR";

                _ar.On();
                _vr.Off();

                _ui.GetComponent<Canvas>().worldCamera = _ar.realityCamera;

                _ui.SetInfo("put");

                _ui.Cam360Mode(realityMode);

            }
            else
            {
                _lastReality = "VR";
                _journeyJustEnd = false;

                _ar.Off();
                _vr.On();

                _ui.GetComponent<Canvas>().worldCamera = _vr.realityCamera;

                _ui.SetInfo("rotate");

                _ui.Cam360Mode(realityMode);   

            }           
        }

        void StopAllReality()
        {
            if(_ui)
                _ui.GetComponent<Canvas>().worldCamera = _ar.realityCamera;
            
            _ar.Off(true);
            _vr.Off();

            _ui.SetInfo();
            _ui.Cam360Mode();

            _wings.EnableAt();
        }

        /// 
        /// Vuforia and Itrackable Methods
        /// 
        public void OnTrackableStateChanged(
                                        TrackableBehaviour.Status previousStatus,
                                        TrackableBehaviour.Status newStatus)
        {
            if (newStatus == TrackableBehaviour.Status.DETECTED ||
                newStatus == TrackableBehaviour.Status.TRACKED ||
                newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
            {
               OnTrackingFound();
            }
            else
            {
                OnTrackingLost();
            }
        }

        private void OnTrackingFound()
        {
            _ui.SetInfo("put");

            Renderer[] components = GetComponentsInChildren<Renderer>();
            foreach (Renderer component in components)
            {
                component.enabled = true;
            }
            SpriteRenderer[] scomponents = GetComponentsInChildren<SpriteRenderer>();
            foreach (SpriteRenderer scomponent in scomponents)
            {
                scomponent.enabled = true;
            }           

            RunReality(_lastReality);
        }

        private void OnTrackingLost()
        {
            _ui.SetInfo();

            StopAllReality();

            Renderer[] components = GetComponentsInChildren<Renderer>();
            foreach (Renderer component in components)
            {
                component.enabled = false;
            }
            SpriteRenderer[] scomponents = GetComponentsInChildren<SpriteRenderer>();
            foreach (SpriteRenderer scomponent in scomponents)
            {
                scomponent.enabled = false;
            }           
        }
    }
}
