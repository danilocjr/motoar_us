﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class MySkyBoxTransform : MonoBehaviour {

    private float adjustX = -90f;
    private float adjustY = 180f;
    private float adjustZ = 0f;

    private bool invX = true;
    private bool invY = false;
    private bool invZ = false;

    private bool _initial = true;

    private Quaternion _lastRotation;
    private Quaternion _currRotation;
   
    public Transform parentObj;

    private void Update()
    {
        
        _currRotation = Quaternion.Euler(parentObj.rotation.eulerAngles.x * (invX ? 1 : -1),
                                         parentObj.rotation.eulerAngles.y * (invY ? 1 : -1), 
                                         parentObj.rotation.eulerAngles.z * (invZ ? 1 : -1));
;

        _currRotation = _currRotation * Quaternion.Euler(adjustX, adjustY, adjustZ); 

        if (_currRotation != _lastRotation)
        {
            if(_initial)
                _initial = false;
            else
                transform.rotation = Quaternion.Lerp(_lastRotation, _currRotation, 0.0000000000000000000000000000000000000000000001f);

            _lastRotation = _currRotation;
        }

    }
}
