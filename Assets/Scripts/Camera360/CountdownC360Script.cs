﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CountdownC360Script : MonoBehaviour {

    public bool done = false;

    private void Awake(){
        StopCount();
    }

    public void StartCount(){
        GetComponent<SpriteRenderer>().enabled = true;
        GetComponent<Animator>().SetBool("start", true);
    }

    public void StopCount(){
        done = false;
        GetComponent<Animator>().SetBool("start", false);
        GetComponent<SpriteRenderer>().enabled = false;
    }

    public void Counting(bool status){
        if (status)
            StartCount();
        else
            StopCount();
    }

    public void CountEnd(){
        done = true;
    }

}
