﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeBehaviour : MonoBehaviour {

    public float velocity = 0.0f;
    private Vector3 _currRotation, _lastRotation;
    private float direction = 0f;

    public Material[]  _materials;
    private int _nextImage = 0;

    public void NextCubemap(){

        _nextImage++;

        if(_nextImage == _materials.Length)
            _nextImage = 0;

        GetComponent<Renderer>().material = _materials[_nextImage];


    }

    /*
   private void LateUpdate()
   {

       transform.Rotate(Vector3.up, velocity * Time.deltaTime);

       _currRotation = GetComponentInParent<RealityComponentsManager>().GetRotation();

       float delta = Mathf.Round(_lastRotation.z) - Mathf.Round(_currRotation.z);
       if (Mathf.Abs(delta) > 2.5f)
       {
           if (delta < -2.5f)
               direction = -1.0f;
           else if (delta > 2.5f)
               direction = 1.0f;

       Debug.Log(_currRotation);

       if ((_currRotation.z < 170f) && (_currRotation.x < 300f))
           direction = -1.0f;
       else if ((_currRotation.z > 200) && (_currRotation.x < 300))
           direction = 1.0f;
       else
           direction = 0;


       _copyTo.Rotate(0f, 25f * direction, 0f);
       transform.rotation = Quaternion.Lerp(_copyFrom.rotation,
                                            _copyTo.rotation,
                                            0.00000001f);
       _lastRotation = _currRotation;
       }

   }
   */
}
