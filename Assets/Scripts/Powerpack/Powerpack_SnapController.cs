﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Vuforia
{
    public class Powerpack_SnapController : MonoBehaviour, ITrackableEventHandler
    {
        private TrackableBehaviour mTrackableBehaviour;

        // Commons
        public string resFolder;
       
        //Reality Controllers
        public RealityComponentsManager _ar;

        // UI elements
        public WingSpriteController _wings;
        public UIManager _ui;
        public UIPowerpackScript _uichild;

        // Private Area
        private Vector3 _targetPosition;
        private Vector3 _targetRotation;

        private string _lastMode = "";
      
        /// 
        /// Monobehaviour Methods
        /// 
        void Start()
        {
            mTrackableBehaviour = GetComponent<TrackableBehaviour>();
            if (mTrackableBehaviour)
            {
                mTrackableBehaviour.RegisterTrackableEventHandler(this);
            }
        }

        void Update()
        {
            if (mTrackableBehaviour.CurrentStatus == TrackableBehaviour.Status.TRACKED)
            {
                _targetRotation = this.transform.localRotation.eulerAngles;
                _ar.UpdatePositionRotation(this.transform.position, _targetRotation);

                _wings.EnableAt(_ar.GetPosition().x);
                   
                if (_ar.GetPosition().x > 0.75f)
                {
                    _ui.PowerpackAnim(true);
                    _ui.SetInfo();
                }

                if(_ui.IsPowerpackAnimEnd())
                {
                    _ui.SetInfo("put");
                    _ui.PowerPackShowMarker(true);
                }
            }
        }

        /// 
        /// Vuforia and Itrackable Methods
        /// 
        public void OnTrackableStateChanged(
                                        TrackableBehaviour.Status previousStatus,
                                        TrackableBehaviour.Status newStatus)
        {
            if (newStatus == TrackableBehaviour.Status.DETECTED ||
                newStatus == TrackableBehaviour.Status.TRACKED ||
                newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
            {
                OnTrackingFound();
            }
            else
            {
                OnTrackingLost();
            }
        }

        private void OnTrackingFound()
        {
            _ui.SetInfo("put");
            _ui.PowerPackShowMarker(true);

            Renderer[] components = GetComponentsInChildren<Renderer>();
            foreach (Renderer component in components)
            {
                component.enabled = true;
            }
            SpriteRenderer[] scomponents = GetComponentsInChildren<SpriteRenderer>();
            foreach (SpriteRenderer scomponent in scomponents)
            {
                scomponent.enabled = false;
            }
          
            _ar.On();
            _ui.GetComponent<Canvas>().worldCamera = _ar.realityCamera;
        }

        private void OnTrackingLost()
        {
            _ui.SetInfo();
            _ui.PowerPackShowMarker(false);
            _ar.Off(true);
            _wings.EnableAt();

            Renderer[] components = GetComponentsInChildren<Renderer>();
            foreach (Renderer component in components)
            {
                component.enabled = false;
            }
            SpriteRenderer[] scomponents = GetComponentsInChildren<SpriteRenderer>();
            foreach (SpriteRenderer scomponent in scomponents)
            {
                scomponent.enabled = false;
            }
        }
    }
}
