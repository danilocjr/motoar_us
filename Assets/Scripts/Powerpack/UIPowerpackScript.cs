﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIPowerpackScript : MonoBehaviour {

    public Animator _anim;

    private void Start()
    {
        _anim.SetBool("start", false);
    }

    public void ShowAnimation(bool status)
    {
        _anim.SetBool("start", status);
    }

    public void EndAnimation()
    {
        _anim.SetBool("start", false);
    }
}
