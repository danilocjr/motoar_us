﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerpackAnimScript : MonoBehaviour {

    public bool isAnimEnd = false;
    public SpriteRenderer _sr;
    //private AudioSource _as;

    public void StartAnim()
    {
        isAnimEnd = false;
    }

    public void EndAnim()
    {
        isAnimEnd = true;
        ShowAnimation(false);
    }

    private void Start()
    {
        GetComponent<Animator>().SetBool("start", false);
        GetComponent<SpriteRenderer>().enabled = false;
        //_as = GetComponent<AudioSource>();
        EnableMask(false);
    }

    public void ShowAnimation(bool status)
    {
        GetComponent<Animator>().SetBool("start", status);
        GetComponent<SpriteRenderer>().enabled = status;
        EnableMask(false);
    }

    public void EnableMask(bool status)
    {
        _sr.enabled = status;
    }

}
