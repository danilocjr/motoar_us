﻿#if UNITY_EDITOR
#pragma warning disable 0219
#endif

using UnityEngine;
using UnityEngine.UI;
using System.Collections;

	public class SetBackgroundTexture : MonoBehaviour {

	private Texture backgroundtexture;
	public static bool recreateGuiTexture = true;

	void LateUpdate() {
		if (recreateGuiTexture)
			StartRecreate ();	
	}


	private void StartRecreate () {
		
		recreateGuiTexture = false;

		if (!backgroundtexture && WebCamTryResolution.webcamTexture) 
		{
			if (!WebCamTryResolution.webcamTexture || !WebCamTryResolution.webcamTexture.isPlaying) 
			{
				GetComponent<RawImage> ().enabled = false;
			}

			Recreate ();
		}
	}

	private void Recreate () {

			float aspect = (float)WebCamTryResolution.webcamTexture.width/(float)WebCamTryResolution.webcamTexture.height;

			backgroundtexture = WebCamTryResolution.webcamTexture;

			GetComponent<RawImage>().enabled = true;
			GetComponent<RawImage>().texture = backgroundtexture;
			GetComponent<RawImage>().rectTransform.sizeDelta = new Vector2(GetComponent<RawImage>().rectTransform.sizeDelta.y * aspect, GetComponent<RawImage>().rectTransform.sizeDelta.y);
		}
}