﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DropboxKit;

public class UploadDemo : MonoBehaviour {

	public Texture2D texture;

	// Use this for initialization
	void Start () {
		byte[] data = texture.EncodeToPNG();

		// with progress
		// DropboxAPI.FilesUpload("SampleImage.PNG", data,
		// delegate(float progress)
		// {
		// 	Debug.Log(progress*100 + "%");
		// },
		// delegate(string json)
		// {
		// 	Debug.Log(json);
		// });

		// without progress
		DropboxAPI.FilesUpload("SampleImage.PNG", data, delegate(string json)
		{
			Debug.Log(json);
		});
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
