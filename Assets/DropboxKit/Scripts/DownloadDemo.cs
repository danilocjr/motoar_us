﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DropboxKit;

public class DownloadDemo : MonoBehaviour {

	public RawImage image;

	// Use this for initialization
	void Start () {
		// with progress
		// DropboxAPI.FilesDownload("/sampleimage.png", delegate(float progress)
		// {
		// 	Debug.Log(progress*100 + "%");
		// },
		// delegate(byte[] data)
		// {
		// 	Texture2D texture = new Texture2D(1, 1);
		// 	texture.LoadImage(data);
		// 	image.texture = texture;
		// 	image.SetNativeSize();
		// });

		// without progress
		DropboxAPI.FilesDownload("/sampleimage.png", delegate(byte[] data){
			Texture2D texture = new Texture2D(1, 1);
			texture.LoadImage(data);
			image.texture = texture;
			image.SetNativeSize();
		});		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
