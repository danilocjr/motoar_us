﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DropboxKit;

public class APICallDemo : MonoBehaviour
{

    DropboxAPI dropbox;

    Texture2D texture;

    public Texture2D downloadImage;

    // Use this for initialization
    void Start()
    {
        #region auth
        // DropboxAPI.Oauth2Authorize();
        // DropboxAPI.Oauth2Token("<code>", delegate (string result)
        // {
        //     Debug.Log(result);
        // });
        // DropboxAPI.Oauth2Token("<code>", delegate (Oauth2TokenResult result)
        // {
        //     if(string.IsNullOrEmpty(result.error))
        //     {
        //          Debug.Log(result.access_token);
        //     }
        //     else
        //     {
        //         Debug.Log(result.error + " - " + result.error_description);
        //     }
        // });        
        // DropboxAPI.TokenFromOauth1("<oauth1_token>", "<oauth1_token_secret>>", delegate(string result)
        // {
        //     Debug.Log(result);
        // });
        // DropboxAPI.TokenFromOauth1("<oauth1_token>", "<oauth1_token_secret>>", delegate (TokenFromOauth1Result result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.oauth2_token);
        //     else
        //         Debug.Log(result.error_summary);
        // });
        // DropboxAPI.TokenRevoke(<access_token>, delegate (string result)
        // {
        //     Debug.Log(result);
        // });
        // DropboxAPI.TokenRevoke("<access_token>", delegate (TokenRevokeResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log("DONE");
        //     else
        //         Debug.Log(result.error_summary);
        // });        
        #endregion

        #region file_properties
        // DropboxAPI.FilePropAdd( "/FolderA", "ptid:1a5n2i6d3OYEAAAAAAAAAYa", "Security Policy", "Confidential", delegate (string result)
        // {
        //     Debug.Log(result);
        // });
        // DropboxAPI.FilePropAdd("/FolderA", "ptid:1a5n2i6d3OYEAAAAAAAAAYa", "Security Policy", "Confidential", delegate (FilePropAddResult result)
        // {
        // if (string.IsNullOrEmpty(result.error_summary))
        //     Debug.Log(result.path);
        // else
        //     Debug.Log(result.error_summary);
        // });
        // DropboxAPI.FilePropOverwrite( "/FolderA", "ptid:1a5n2i6d3OYEAAAAAAAAAYa", "Security Policy", "Confidential", delegate (string result)
        // {
        //     Debug.Log(result);
        // });   
        // DropboxAPI.FilePropOverwrite( "/FolderA", "ptid:1a5n2i6d3OYEAAAAAAAAAYa", "Security Policy", "Confidential", delegate (FilePropOverwriteResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.path);
        //     else
        //         Debug.Log(result.error_summary);
        // });           
        // DropboxAPI.FilePropRemove( "/FolderA", "ptid:1a5n2i6d3OYEAAAAAAAAAYa", delegate (string result)
        // {
        //     Debug.Log(result);
        // });   
        // DropboxAPI.FilePropRemove("/FolderA", "ptid:1a5n2i6d3OYEAAAAAAAAAYa", delegate (FilePropRemoveResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log("DONE");
        //     else
        //         Debug.Log(result.error_summary);
        // });
        // DropboxAPI.FilePropSearch( "Confidential", "Security", "or_operator", "filter_none", delegate (string result)
        // {
        //     Debug.Log(result);
        // });     
        // DropboxAPI.FilePropSearch( "Confidential", "Security", "or_operator", "filter_none", delegate (FilePropSearchResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log("Count:"+result.matches.Count);
        //     else
        //         Debug.Log(result.error_summary);
        // });             
        // DropboxAPI.FilePropSearchContinue("ZtkX9_EHj3x7PMkVuFIhwKYXEpwpLwyxp9vMKomUhllil9q7eWiAu", delegate (string result)
        // {
        //     Debug.Log(result);
        // });
        // DropboxAPI.FilePropSearchContinue("ZtkX9_EHj3x7PMkVuFIhwKYXEpwpLwyxp9vMKomUhllil9q7eWiAu", delegate (FilePropSearchContinueResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log("Count:" + result.matches.Count);
        //     else
        //         Debug.Log(result.error_summary);
        // });
        // DropboxAPI.FilePropUpdate( "/FolderA", "ptid:1a5n2i6d3OYEAAAAAAAAAYa", "Security Policy", "Confidential", delegate (string result)
        // {
        //     Debug.Log(result);
        // });
        // DropboxAPI.FilePropUpdate("/FolderA", "ptid:1a5n2i6d3OYEAAAAAAAAAYa", "Security Policy", "Confidential", delegate (FilePropUpdateResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log("DONE");
        //     else
        //         Debug.Log(result.error_summary);
        // });
        // DropboxAPI.FileTempAddForUser("Security", "<Description>", "Security Policy", "<Description>", "string", delegate ( string result)
        // {
        //     Debug.Log(result);
        // });
        // DropboxAPI.FileTempAddForUser("Security", "<Description>", "Security Policy", "<Description>", "string", delegate ( FileTempAddForUserResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log("DONE");
        //     else
        //         Debug.Log(result.error_summary);
        // });        
        // DropboxAPI.FileTempGetForUser("ptid:1a5n2i6d3OYEAAAAAAAAAYa", delegate (string result)
        // {
        //     Debug.Log(result);
        // });
        // DropboxAPI.FileTempGetForUser("ptid:1a5n2i6d3OYEAAAAAAAAAYa", delegate (FileTempGetForUserResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log("DONE");
        //     else
        //         Debug.Log(result.error_summary);
        // });
        // DropboxAPI.FileTempListForUser(delegate (string result)
        // {
        //      Debug.Log(result);
        // });
        // DropboxAPI.FileTempListForUser(delegate (FileTempListForUserResult result)
        // {
        //      if (string.IsNullOrEmpty(result.error_summary))
        //          Debug.Log("DONE");
        //      else
        //          Debug.Log(result.error_summary);
        // });
        // DropboxAPI.FileTempRemoveForUser("ptid:1a5n2i6d3OYEAAAAAAAAAYa", delegate (string result)
        // {
        //     Debug.Log(result);
        // }); 
        // DropboxAPI.FileTempRemoveForUser("ptid:1a5n2i6d3OYEAAAAAAAAAYa", delegate (FileTempRemoveForUserResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log("DONE");
        //     else
        //         Debug.Log(result.error_summary);
        // });                
        // DropboxAPI.FileTempUpdateForUser("ptid:1a5n2i6d3OYEAAAAAAAAAYa", "New Security Template Name", "These properties will describe how confidential this file or folder is.", "Security Policy", "This is the security policy of the file or folder described.\nPolicies can be Confidential, Public or Internal.", "string", delegate (string result)
        // {
        //     Debug.Log(result);
        // }); 
        // DropboxAPI.FileTempUpdateForUser("ptid:1a5n2i6d3OYEAAAAAAAAAYa", "New Security Template Name", "These properties will describe how confidential this file or folder is.", "Security Policy", "This is the security policy of the file or folder described.\nPolicies can be Confidential, Public or Internal.", "string", delegate (FileTempUpdateForUserResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log("DONE");
        //     else
        //         Debug.Log(result.error_summary);
        // });
        #endregion

        #region files
        // DropboxAPI.FilesCopyBatch(new string[]{"/homework", "/homework2"}, new string[]{"/homeworkA1", "/homeworkA2"}, delegate (string result)
        // {
        //     Debug.Log(result);
        // });
        // DropboxAPI.FilesCopyBatch(new string[] { "/homework", "/homework2" }, new string[] { "/homeworkA1", "/homeworkA2" }, delegate (FilesCopyBatchResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.async_job_id);
        //     else
        //         Debug.Log(result.error_summary);
        // });

        // DropboxAPI.FilesCopyBatchCheck("dbjid:AAA5lQU9cImPnXlw23pNoK0eQo62ljVG7fkqx_e1tAjromQyn6lZnV5rJ_z6VBSBxFVUwuxixQgtd6ddRCOK7AB6", delegate(string result)
        // {
        //     Debug.Log(result);
        // });
        // DropboxAPI.FilesCopyBatchCheck("dbjid:AAA5lQU9cImPnXlw23pNoK0eQo62ljVG7fkqx_e1tAjromQyn6lZnV5rJ_z6VBSBxFVUwuxixQgtd6ddRCOK7AB6", delegate (FilesCopyBatchCheckResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.tag);
        //     else
        //         Debug.Log(result.error_summary);
        // });

        // DropboxAPI.FilesCopyReferenceGet("/20171113_172837.jpg", delegate (string result)
        //  {
        //      Debug.Log(result);
        //  });
        // DropboxAPI.FilesCopyReferenceGet("/20171113_172837.jpg", delegate (FilesCopyReferenceGetResult result)
        //  {
        //      if (string.IsNullOrEmpty(result.error_summary))
        //          Debug.Log(result.expires);
        //      else
        //          Debug.Log(result.error_summary);
        //  });

        // DropboxAPI.FilesCopyReferenceSave("refid", "/20171113_172837.jpg", delegate (string result)
        //  {
        //      Debug.Log(result);
        //  });
        // DropboxAPI.FilesCopyReferenceSave("refid", "/20171113_172837.jpg", delegate (FilesCopyReferenceSaveResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.metadata.id);
        //     else
        //         Debug.Log(result.error_summary);
        // });

        // DropboxAPI.FilesCopyv2("/20171113_172837.jpg", "/20171113_172837_v2.jpg", delegate (string result)
        // {
        //     Debug.Log(result);
        // });
        // DropboxAPI.FilesCopyv2("/20171113_172837.jpg", "/20171113_172837_v2.jpg", delegate (FilesCopyv2Result result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.metadata.id);
        //     else
        //         Debug.Log(result.error_summary);
        // });

        // DropboxAPI.FilesCreateFolderv2("/NewFolder", delegate(string result)
        // {
        //     Debug.Log(result);
        // });
        // DropboxAPI.FilesCreateFolderv2("/NewFolder", delegate (FilesCreateFolderv2Result result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.metadata.id);
        //     else
        //         Debug.Log(result.error_summary);
        // });

        // DropboxAPI.FilesDeleteBatch(new string[]{"/homeworkA1", "/homeworkA2"}, delegate(string result)
        // {
        //     Debug.Log(result);
        // });
        // DropboxAPI.FilesDeleteBatch(new string[]{"/homeworkA1", "/homeworkA2"}, delegate(FilesDeleteBatchResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.tag);
        //     else
        //         Debug.Log(result.error_summary);
        // });        
        // DropboxAPI.FilesDeleteBatchCheck("dbjid:AACNH3D8xf6ST_uKIRW_mvJH_EyDQNJwInhlhef6A8BqQbkRW_7jqCtwfyJssL-jSDnE-R8DuBdK613W1V2pPj1C", delegate(string result)
        // {
        //     Debug.Log(result);
        // });
        // DropboxAPI.FilesDeleteBatchCheck("dbjid:AACNH3D8xf6ST_uKIRW_mvJH_EyDQNJwInhlhef6A8BqQbkRW_7jqCtwfyJssL-jSDnE-R8DuBdK613W1V2pPj1C", delegate(FilesDeleteBatchResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.tag);
        //     else
        //         Debug.Log(result.error_summary);
        // });
        // DropboxAPI.FilesDeletev2("/20171113_172837_v2jpg", delegate(string result)
        // {
        //     Debug.Log(result);
        // });
        // DropboxAPI.FilesDeletev2("/20171117_105727.jpg", delegate(FilesDeletev2Result result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.metadata.id);
        //     else
        //         Debug.Log(result.error_summary);
        // });        
        // DropboxAPI.FilesDownload("20171113_172837.jpg", delegate(byte[] result)
        // {
        //     downloadImage = new Texture2D(1, 1);
        //     downloadImage.LoadImage(result, false);
        // }, delegate(string error)
        // {
        //     Debug.Log(error);
        // }); 
        // DropboxAPI.FilesDownloadZip("/homework", delegate(byte[] result)
        // {
        //     System.IO.File.WriteAllBytes(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Desktop) + "/test.zip", result);
        // }, delegate (string error)
        // {
        //     Debug.Log(error);
        // });                             
        // DropboxAPI.FilesGetMetadata("/20171113_172837_v2.jpg", delegate(string result)
        // {
        //     Debug.Log(result);
        // });
        // DropboxAPI.FilesGetMetadata("/20171113_172837_v2.jpg", delegate(FilesGetMetadataResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.tag);
        //     else
        //         Debug.Log(result.error_summary);
        // });        
        // DropboxAPI.FilesGetPreview("/test.rtf", delegate (byte[] result)
        // {
        //     System.IO.File.WriteAllBytes(Application.streamingAssetsPath + "/test.pdf", result);
        // }, delegate (string error)
        // {
        //     Debug.Log(error);
        // });
        // DropboxAPI.FilesGetTemporaryLink("/test.rtf", delegate(string result)
        // {
        //     Debug.Log(result);
        // });
        // DropboxAPI.FilesGetTemporaryLink("/test.rtf", delegate(FilesGetTemporaryLinkResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.link);
        //     else
        //         Debug.Log(result.error_summary);
        // });
        // DropboxAPI.FilesGetThumbnail("/20171113_172837_v2.jpg", "jpeg", "w128h128", delegate(byte[] result)
        // {
        //     downloadImage = new Texture2D(1, 1);
        //     downloadImage.LoadImage(result, false);
        // }, delegate (string error)
        // {
        //     Debug.Log(error);
        // });

        // DropboxAPI.FilesGetThumbnailBatch(new string[]{"/20171113_172837_v2.jpg", "/20171113_172837.jpg"}, new string[]{"jpeg", "jpeg"}, new string[]{"w64h64", "w128h128"}, delegate(FilesGetThumbnailBatchResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.entries[0].tag);
        //     else
        //         Debug.Log(result.error_summary);
        // });       
        // DropboxAPI.FilesListFolder("/homework", delegate(string result)
        // {
        //     Debug.Log(result);
        // }); 
        // DropboxAPI.FilesListFolder("/homework", delegate(FilesListFolderResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.entries[0].tag);
        //     else
        //         Debug.Log(result.error_summary);
        // });         
        // DropboxAPI.FilesListFolderContinue("AAGRZ9zoy7YrEugqJLXpfZ5_wUv-b1uwawnRDNj1p4c4QPde3WAB7yCtrmSEwUJuBnP4fzJ8EwUEixpP6hbgmQpP8Iw1H_GLYjAKHU86fnhPE661K8sAmT7QtpYhkqGw9hzL6Lq01vQE8CCVJ5cFMkrNtizObYNhYTc0cV4trg2kHw", delegate (string result)
        // {
        //     Debug.Log(result);
        // });
        // DropboxAPI.FilesListFolderContinue("AAGRZ9zoy7YrEugqJLXpfZ5_wUv-b1uwawnRDNj1p4c4QPde3WAB7yCtrmSEwUJuBnP4fzJ8EwUEixpP6hbgmQpP8Iw1H_GLYjAKHU86fnhPE661K8sAmT7QtpYhkqGw9hzL6Lq01vQE8CCVJ5cFMkrNtizObYNhYTc0cV4trg2kHw", delegate (FilesListFolderContinueResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.entries[0].tag);
        //     else
        //         Debug.Log(result.error_summary);
        // });        
        // DropboxAPI.FilesListFolderGetLatestCursor("/homework", delegate(string result)
        // {
        //     Debug.Log(result);
        // });     
        // DropboxAPI.FilesListFolderGetLatestCursor("/homework", delegate(FilesListFolderGetLatestCursorResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.cursor);
        //     else
        //         Debug.Log(result.error_summary);
        // });        
        // DropboxAPI.FilesListFolderLongPoll("AAFi_qkn5m5NPuj27MSJt5hTbaK80KGVgaOl7hVvisnqevBY1veRMBCpPHKgJEMhshVSwSTFVWQyKA7NGt3seAHqf-5Yc4zAe9ow0yBkgwj-2xN8P5hjXVz7SxY7znKyQVzikAhy-1uKgqLmcaRIBmIPScAikzM7vEIrscFTfOSd2Q", delegate (string result)
        // {
        //     Debug.Log(result);
        // });    
        // DropboxAPI.FilesListFolderLongPoll("AAFi_qkn5m5NPuj27MSJt5hTbaK80KGVgaOl7hVvisnqevBY1veRMBCpPHKgJEMhshVSwSTFVWQyKA7NGt3seAHqf-5Yc4zAe9ow0yBkgwj-2xN8P5hjXVz7SxY7znKyQVzikAhy-1uKgqLmcaRIBmIPScAikzM7vEIrscFTfOSd2Q", delegate (FilesListFolderLongPollResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.changes);
        //     else
        //         Debug.Log(result.error_summary);
        // });          
        // DropboxAPI.FilesListRevisions("/20171113_172837_v2.jpg", delegate (string result)
        // {
        //     Debug.Log(result);
        // }); 
        // DropboxAPI.FilesListRevisions("/20171113_172837_v2.jpg", delegate (FilesListRevisionsResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.entries[0].rev);
        //     else
        //         Debug.Log(result.error_summary);
        // });          
        // DropboxAPI.FilesMoveBatch(new string[]{"/test.pdf"}, new string []{"/FolderA/test.pdf"}, delegate(string result)
        // {
        //     Debug.Log(result);
        // });
        // DropboxAPI.FilesMoveBatch(new string[]{"/test.pdf"}, new string []{"/FolderA/test.pdf"}, delegate(FilesMoveBatchResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.entries.Count);
        //     else
        //         Debug.Log(result.error_summary);
        // });        
        // DropboxAPI.FilesMoveBatchCheck("dbjid:AAB6jdOZgneFcaXbjbK0wkKobbohkw24M6FTKSwGPfYs15voh4voq5M0wozSkJmDzlYJqmP3zcJ8oSRFYRmFsJFA", delegate (string result)
        // {
        //     Debug.Log(result);
        // });
        // DropboxAPI.FilesMoveBatchCheck("dbjid:AAB6jdOZgneFcaXbjbK0wkKobbohkw24M6FTKSwGPfYs15voh4voq5M0wozSkJmDzlYJqmP3zcJ8oSRFYRmFsJFA", delegate (FilesMoveBatchCheckResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.tag);
        //     else
        //         Debug.Log(result.error_summary);
        // });        
        // DropboxAPI.FilesMovev2("/test.txt", "/FolderA/test.txt", delegate (string result)
        // {
        //     Debug.Log(result);
        // });
        // DropboxAPI.FilesMovev2("/FolderA/test.txt", "/test.txt", delegate (FilesMovev2Result result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.metadata.id);
        //     else
        //         Debug.Log(result.error_summary);
        // });        
        // DropboxAPI.FilesPermanentlyDelete("/20171113_172837_v2.jpg", delegate(string result)
        // {
        //     Debug.Log(result);
        // });
        // DropboxAPI.FilesPermanentlyDelete("/20171113_172837_v2.jpg", delegate (FilesPermanentlyDeleteResult result)
        // {
        //     if (result == null)
        //         Debug.Log("DONE");
        //     else
        //         Debug.Log(result.error_summary);
        // });
        // DropboxAPI.FilesRestore("/20171113_172837_v2.jpg", "325e459cbb", delegate (string result)
        // {
        //     Debug.Log(result);
        // });
        // DropboxAPI.FilesRestore("/20171113_172837_v2.jpg", "325e459cbb", delegate (FilesRestoreResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log("DONE");
        //     else
        //         Debug.Log(result.error_summary);
        // });
        // DropboxAPI.FilesSaveURL("/balloon.jpg", "https://pixabay.com/get/e836b60c29f2003ed1534705fb0938c9bd22ffd41cb0114995f4c578a5/balloon-1373161_1920.jpg", delegate (string result)
        // {
        //     Debug.Log(result);
        // });
        // DropboxAPI.FilesSaveURL("/balloon.jpg", "https://pixabay.com/get/e836b60c29f2003ed1534705fb0938c9bd22ffd41cb0114995f4c578a5/balloon-1373161_1920.jpg", delegate (FilesSaveURLResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.async_job_id);
        //     else
        //         Debug.Log(result.error_summary);
        // });
        // DropboxAPI.FilesSaveURLCheckJobStatus("bkTGwl8q8aAAAAAAAAAAPg", delegate (string result )
        // {
        //     Debug.Log(result);
        // });
        // DropboxAPI.FilesSaveURLCheckJobStatus("bkTGwl8q8aAAAAAAAAAAPg", delegate (FilesSaveURLCheckJobStatusResult result )
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.tag);
        //     else
        //         Debug.Log(result.error_summary);
        // });        
        // DropboxAPI.FilesSearch("", "balloon", delegate (string result)
        // {
        //     Debug.Log(result);
        // });
        // DropboxAPI.FilesSearch("", "balloon", delegate (FilesSearchResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.matches.Count);
        //     else
        //         Debug.Log(result.error_summary);
        // });        

        // string date = System.DateTime.Now.ToString("yyyyMMdd_HHmmss");
        // Texture2D texture2d = new Texture2D(100, 100);
        // byte[] data = texture2d.EncodeToJPG();
        // string fileName = date + ".jpg";
        // DropboxAPI.FilesUpload(fileName, data, delegate (string uploadResult)
        // {
        //     Debug.Log(uploadResult);
        // });  
        // DropboxAPI.FilesUpload(fileName, data, delegate (FilesUploadResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.id);
        //     else
        //         Debug.Log(result.error_summary);
        // });          

        // DropboxAPI.FilesUploadSessionStart( false, delegate (string result)
        // {
        //     Debug.Log(result);
        // });   

        // string date = System.DateTime.Now.ToString("yyyyMMdd_HHmmss");
        // Texture2D texture2d = new Texture2D(100, 100);
        // byte[] data = texture2d.EncodeToJPG();
        // string fileName = date + ".jpg";
        // DropboxAPI.FilesUploadSessionFinish("AAAAAAAAAEN5xIpzb1oU6Q", 0, "/streamingFile.jpg", data, delegate (string result)
        // {
        //     Debug.Log(result);
        // });
        // DropboxAPI.FilesUploadSessionFinish("AAAAAAAAAEN5xIpzb1oU6Q", 0, "/streamingFile.jpg", data, delegate (FilesUploadSessionFinishResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.id);
        //     else
        //         Debug.Log(result.error_summary);
        // });        

        // DropboxAPI.FilesUploadSessionFinishBatch(new string[]{"AAAAAAAAAEVcreTrCNCNhg", "AAAAAAAAAEbn45RDmAwD1A"}, new int[]{0, 0}, new string[]{"/streamA.txt", "/streamB.txt"}, delegate(string result)
        // {
        //     Debug.Log(result);
        // });        

        // DropboxAPI.FilesUploadSessionFinishBatchCheck("dbjid:AADbITUit2QscElDTg-DqL0PAkMoDch5736wX8ClXIua1UUPDa7s761b5EO8HgtLSFCvtMCTYNQ3Idi9TQg-2fuf", delegate(string result)
        // {
        //     Debug.Log(result);
        // });
        // DropboxAPI.FilesUploadSessionFinishBatchCheck("dbjid:AADbITUit2QscElDTg-DqL0PAkMoDch5736wX8ClXIua1UUPDa7s761b5EO8HgtLSFCvtMCTYNQ3Idi9TQg-2fuf", delegate(FilesUploadSessionFinishBatchCheckResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.tag);
        //     else
        //         Debug.Log(result.error_summary);
        // });        

        #endregion

        #region users
        // DropboxAPI.GetAccount("dbid:AADzCXfqvWP8-MzFpllDVWcC-a9l3iC9EO4", delegate(string result)
        // {
        //     Debug.Log(result);
        // });
        // DropboxAPI.GetAccount("dbid:AADzCXfqvWP8-MzFpllDVWcC-a9l3iC9EO4", delegate(GetAccountResult result)
        // {
        //     if(string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.name.display_name);
        //     else
        //         Debug.Log(result.error_summary);
        // });      
        // DropboxAPI.GetAccountBatch(new string[]{"dbid:AADzCXfqvWP8-MzFpllDVWcC-a9l3iC9EO4"}, delegate(string result)
        // {
        //     Debug.Log(result);
        // });
        // DropboxAPI.GetAccountBatch(new string[]{"dbid:AADzCXfqvWP8-MzFpllDVWcC-a9l3iC9EO4"}, delegate(GetAccountBatchResult result)
        // {
        //     if(string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.accounts.Count);
        //     else
        //         Debug.Log(result.error_summary);
        // });        
        // DropboxAPI.GetCurrentAccount(delegate(string result)
        // {
        //     Debug.Log(result);
        // });     
        // DropboxAPI.GetCurrentAccount(delegate(GetCurrentAccountResult result)
        // {
        //     if(string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.name.display_name);
        //     else
        //         Debug.Log(result.error_summary);
        // });     

        // DropboxAPI.GetSpaceUsage(delegate(string result)
        // {
        //     Debug.Log(result);
        // }); 
        // DropboxAPI.GetSpaceUsage(delegate(GetSpaceUsageResult result)
        // {
        //     if(string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.used + "/" + result.allocation.allocated);
        //     else
        //         Debug.Log(result.error_summary);
        // });                     
        #endregion

        #region file_requests
        // DropboxAPI.FileRequestsCreate("Title", "/Sharefolder", true, delegate(string result)
        // {
        //     Debug.Log(result);
        // });

        // DropboxAPI.FileRequestsCreate("Title", "/Sharefolder", true, delegate (FileRequestsCreateResult result)
        // {
        //     if(string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.id);
        //     else
        //         Debug.Log(result.error_summary);
        // });

        // DropboxAPI.FileRequestsGet("lGjAehdxkQit4uTqRC19", delegate(string result)
        // {
        //     Debug.Log(result);
        // }); 
        // DropboxAPI.FileRequestsGet("QDM1wJ88a9EYaV6eDtFY", delegate(FileRequestsGetResult result)
        // {
        //     if(string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.id);
        //     else
        //         Debug.Log(result.error_summary);
        // });         

        // DropboxAPI.FileRequestsList(delegate(string result)
        // {
        //     Debug.Log(result);
        // });   
        // DropboxAPI.FileRequestsList(delegate(FileRequestsListResult result)
        // {
        //     if(string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.file_requests.Count.ToString());
        //     else
        //         Debug.Log(result.error_summary);
        // });         

        // DropboxAPI.FileRequestsUpdate("pm40YBHI7EizomnCX0C7", "Homework", "/homework", false, delegate(string result)
        // {
        //     Debug.Log(result);
        // });       
        // DropboxAPI.FileRequestsUpdate("pm40YBHI7EizomnCX0C7", "Homework", "/homework", false, delegate (FileRequestsUpdateResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.id);
        //     else
        //         Debug.Log(result.error_summary);
        // });
        #endregion

        #region Paper
        // DropboxAPI.DocsArchive("uaSvRuxvnkFa12PTkBv5q", delegate(string result)
        // {
        //     Debug.Log(result);
        // });
        //  DropboxAPI.DocsArchive("uaSvRuxvnkFa12PTkBv5q", delegate(DocsArchiveResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log("DONE");
        //     else
        //         Debug.Log(result.error_summary);
        // });       
        // string html = "<html></html>";
        // byte[] bytes = System.Text.Encoding.ASCII.GetBytes(html);
        // DropboxAPI.DocsCreate("html", bytes, delegate(string result)
        // {
        //     Debug.Log(result);
        // });
        // DropboxAPI.DocsCreate("html", bytes, delegate(DocsCreateResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.doc_id);
        //     else
        //         Debug.Log(result.error_summary);
        // });        
        // DropboxAPI.DocsDownload("uaSvRuxvnkFa12PTkBv5q", "markdown", delegate(byte[] result)
        // {
        //     Debug.Log(result);
        //     byte[] bytes = result;
        // });
        // DropboxAPI.DocsDownload("uaSvRuxvnkFa12PTkBv5q", "markdown", delegate(byte[] result)
        // {
        //     Debug.Log(result.Length);
        // }, delegate(string error)
        // {
        //     Debug.Log(error);
        // });        
        // DropboxAPI.DocsFolderUsersList("uaSvRuxvnkFa12PTkBv5q", delegate (string result)
        // {
        //     Debug.Log(result);
        // });
        // DropboxAPI.DocsFolderUsersList("uaSvRuxvnkFa12PTkBv5q", delegate (DocsFolderUsersListResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.has_more);
        //     else
        //         Debug.Log(result.error_summary);
        // });        
        // DropboxAPI.DocsFolderUsersListContinue("uaSvRuxvnkFa12PTkBv5q", "U60b6BxT43ySd5sAVQbbIvoteSnWLjUdLU7aR25hbt3ySd5sAVQbbIvoteSnWLjUd", delegate (string result)
        // {
        //     Debug.Log(result);
        // }); 
        // DropboxAPI.DocsFolderUsersListContinue("uaSvRuxvnkFa12PTkBv5q", "U60b6BxT43ySd5sAVQbbIvoteSnWLjUdLU7aR25hbt3ySd5sAVQbbIvoteSnWLjUd", delegate (DocsFolderUsersListContinueResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.has_more);
        //     else
        //         Debug.Log(result.error_summary);
        // });         
        // DropboxAPI.DocsGetFolderInfo("uaSvRuxvnkFa12PTkBv5q", delegate (string result)
        // {
        //     Debug.Log(result);
        // });
        // DropboxAPI.DocsGetFolderInfo("uaSvRuxvnkFa12PTkBv5q", delegate (DocsGetFolderInfoResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.folders.Count);
        //     else
        //         Debug.Log(result.error_summary);
        // });
        // DropboxAPI.DocsList("docs_created", "modified", "descending", delegate (string result)
        // {
        //     Debug.Log(result);
        // });  
        // DropboxAPI.DocsList("docs_created", "modified", "descending", delegate (DocsListResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.doc_ids.Count);
        //     else
        //         Debug.Log(result.error_summary);
        // });          
        // DropboxAPI.DocsListContinue("U60b6BxT43ySd5sAVQbbIvoteSnWLjUdLU7aR25hbt3ySd5sAVQbbIvoteSnWLjUd", delegate (string result)
        // {
        //     Debug.Log(result);
        // });
        // DropboxAPI.DocsListContinue("U60b6BxT43ySd5sAVQbbIvoteSnWLjUdLU7aR25hbt3ySd5sAVQbbIvoteSnWLjUd", delegate (DocsListContinueResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.doc_ids.Count);
        //     else
        //         Debug.Log(result.error_summary);
        // });        
        // DropboxAPI.DocsPermanentlyDelete("uaSvRuxvnkFa12PTkBv5q", delegate (string result)
        // {
        //     Debug.Log(result);
        // });
        // DropboxAPI.DocsPermanentlyDelete("uaSvRuxvnkFa12PTkBv5q", delegate (DocsPermanentlyDeleteResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log("DONE");
        //     else
        //         Debug.Log(result.error_summary);
        // });        
        // DropboxAPI.DocsSharingPolicyGet("uaSvRuxvnkFa12PTkBv5q", delegate (string result)
        // {
        //     Debug.Log(result);
        // });        
        // DropboxAPI.DocsSharingPolicyGet("uaSvRuxvnkFa12PTkBv5q", delegate (DocsSharingPolicyGetResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.public_sharing_policy.tag);
        //     else
        //         Debug.Log(result.error_summary);
        // });           
        // DropboxAPI.DocsSharingPolicySet("uaSvRuxvnkFa12PTkBv5q", "people_with_link_can_edit", "people_with_link_can_edit", delegate (string result)
        // {
        //     Debug.Log(result);
        // }); 
        // DropboxAPI.DocsSharingPolicySet("uaSvRuxvnkFa12PTkBv5q", "people_with_link_can_edit", "people_with_link_can_edit", delegate (DocsSharingPolicySetResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.doc_id);
        //     else
        //         Debug.Log(result.error_summary);
        // });         
        string html = "<html></html>";
        byte[] bytes = System.Text.Encoding.ASCII.GetBytes(html);
        // DropboxAPI.DocsUpdate(bytes, "uaSvRuxvnkFa12PTkBv5q", "prepend", 56556, "plain_text", delegate (string result)
        // {
        //     Debug.Log(result);
        // });
        // DropboxAPI.DocsUpdate(bytes, "uaSvRuxvnkFa12PTkBv5q", "prepend", 56556, "plain_text", delegate (DocsUpdateResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.doc_id);
        //     else
        //         Debug.Log(result.error_summary);
        // });        
        // DropboxAPI.DocsUsersAdd("uaSvRuxvnkFa12PTkBv5q", "justin@example.com", "view_and_comment", "Welcome to Paper.", false, delegate (string result)
        // {
        //     Debug.Log(result);
        // });  
        // DropboxAPI.DocsUsersAdd("uaSvRuxvnkFa12PTkBv5q", "justin@example.com", "view_and_comment", "Welcome to Paper.", false, delegate (DocsUsersAddResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log("DONE");
        //     else
        //         Debug.Log(result.error_summary);
        // });           
        // DropboxAPI.DocsUserList("uaSvRuxvnkFa12PTkBv5q", "shared", delegate (string result)
        // {
        //     Debug.Log(result);
        // }); 
        // DropboxAPI.DocsUserList("uaSvRuxvnkFa12PTkBv5q", "shared", delegate (DocsUserListResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.has_more);
        //     else
        //         Debug.Log(result.error_summary);
        // });                         
        // DropboxAPI.DocsUserListContinue("uaSvRuxvnkFa12PTkBv5q", "U60b6BxT43ySd5sAVQbbIvoteSnWLjUdLU7aR25hbt3ySd5sAVQbbIvoteSnWLjUd", delegate (string result)
        // {
        //     Debug.Log(result);
        // });  
        // DropboxAPI.DocsUserListContinue("uaSvRuxvnkFa12PTkBv5q", "U60b6BxT43ySd5sAVQbbIvoteSnWLjUdLU7aR25hbt3ySd5sAVQbbIvoteSnWLjUd", delegate (DocsUserListContinueResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.has_more);
        //     else
        //         Debug.Log(result.error_summary);
        // });         
        // DropboxAPI.DocsUsersRemove("uaSvRuxvnkFa12PTkBv5q", "justin@example.com", delegate (string result)
        // {
        //     Debug.Log(result);
        // });
        // DropboxAPI.DocsUsersRemove("uaSvRuxvnkFa12PTkBv5q", "justin@example.com", delegate (DocsUsersRemoveResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log("DONE");
        //     else
        //         Debug.Log(result.error_summary);
        // });                                    
        #endregion

        #region sharing
        // DropboxAPI.SharingAddFileMember("id:0-KNFfp-7xAAAAAAAAAANg", new string[]{"cchiutse@hotmail.com"}, "Share with you.", false, "viewer", delegate(string result)
        // {
        //     Debug.Log(result);
        // });
        // DropboxAPI.SharingAddFileMember("id:0-KNFfp-7xAAAAAAAAAANg", new string[]{"cchiutse@hotmail.com"}, "Share with you.", false, "viewer", delegate(SharingAddFileMemberResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log("DONE");
        //     else
        //         Debug.Log(result.error_summary);
        // });        
        // DropboxAPI.SharingAddFolderMember("/InfoFile", new string[]{"cchiutse@hotmail.com"}, "Share folder with you.", false,  new string[]{"viewer"}, delegate(string result)
        // {
        //     Debug.Log(result);
        // });
        // DropboxAPI.SharingAddFolderMember("/InfoFile", new string[]{"cchiutse@hotmail.com"}, "Share folder with you.", false,  new string[]{"viewer"}, delegate(SharingAddFolderMemberResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log("DONE");
        //     else
        //         Debug.Log(result.error_summary);
        // });        
        // DropboxAPI.SharingCheckJobStatus("id:0-KNFfp-7xAAAAAAAAAAIA", delegate (string result)
        // {
        //     Debug.Log(result);
        // });
        // DropboxAPI.SharingCheckJobStatus("id:0-KNFfp-7xAAAAAAAAAAIA", delegate (SharingCheckJobStatusResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.async_job_id);
        //     else
        //         Debug.Log(result.error_summary);
        // });        
        // DropboxAPI.SharingCheckRemoveMemberJobStatus("<id>", delegate (string result)
        // {
        //     Debug.Log(result);
        // }); 
        // DropboxAPI.SharingCheckRemoveMemberJobStatus("<id>", delegate (SharingCheckRemoveMemberJobStatusStatus result)
        // {
        //      if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.tag);
        //     else
        //         Debug.Log(result.error_summary);
        // });          
        // DropboxAPI.SharingCheckShareJobStatus("<id>", delegate (string result)
        //  {
        //      Debug.Log(result);
        //  });
        // DropboxAPI.SharingCheckShareJobStatus("<id>", delegate (SharingCheckShareJobStatusResult result)
        // {
        //      if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.tag);
        //     else
        //         Debug.Log(result.error_summary);
        // });        
        // DropboxAPI.SharingCreateSharedLinkWithSettings("/homework", delegate (string result)
        // {
        //     Debug.Log(result);
        // });
        // DropboxAPI.SharingCreateSharedLinkWithSettings("/homework", delegate (SharingCreateSharedLinkWithSettingsResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.tag);
        //     else
        //         Debug.Log(result.error_summary);
        // });
        // DropboxAPI.SharingGetFileMetadata("id:0-KNFfp-7xAAAAAAAAAAIA", delegate (string result)
        // {
        //     Debug.Log(result);
        // });
        // DropboxAPI.SharingGetFileMetadata("id:0-KNFfp-7xAAAAAAAAAAIA", delegate (SharingGetFileMetadataResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.id);
        //     else
        //         Debug.Log(result.error_summary);
        // });

        // DropboxAPI.SharingGetFileMetadataBatch(new string[]{"id:0-KNFfp-7xAAAAAAAAAAIA"}, delegate (string result)
        // {
        //     Debug.Log(result);
        // });
        // DropboxAPI.SharingGetFileMetadataBatch(new string[]{"id:0-KNFfp-7xAAAAAAAAAAIA"}, delegate (SharingGetFileMetadataBatchResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log("DONE");
        //     else
        //         Debug.Log(result.error_summary);
        // });        

        // DropboxAPI.SharingGetFolderMetadata("84528192421", delegate (string result)
        // {
        //     Debug.Log(result);
        // });
        // DropboxAPI.SharingGetFolderMetadata("84528192421", delegate (SharingGetFolderMetadataResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.name);
        //     else
        //         Debug.Log(result.error_summary);
        // });

        // DropboxAPI.SharingGetSharedLinkFile("https://www.dropbox.com/s/qj5ecnxam7ob7rf/20171113_172837.jpg?dl=0", delegate(byte[] result)
        // {
        //     if(result != null)
        //     {
        //         Texture2D texture = new Texture2D(1, 1);
        //         texture.LoadImage(result);
        //         texture.Apply();
        //      downloadImage = texture;
        //     }
        // });


        // DropboxAPI.SharingGetSharedLinkFile("https://www.dropbox.com/s/qj5ecnxam7ob7rf/20171113_172837.jpg?dl=0", delegate(byte[] result)
        // {
        //     Texture2D texture = new Texture2D(1, 1);
        //     texture.LoadImage(result);
        //     texture.Apply();
        //     downloadImage = texture;
        // }, delegate (string error)
        // {
        //    Debug.Log(error); 
        // });   
        // DropboxAPI.SharingGetSharedLinkFile("https://www.dropbox.com/s/qj5ecnxam7ob7rf/20171113_172837.jpg?dl=0", delegate(byte[] result)
        // {
        //     Texture2D texture = new Texture2D(1, 1);
        //     texture.LoadImage(result);
        //     texture.Apply();
        //      downloadImage = texture;
        // });        
        // DropboxAPI.SharingGetSharedLinkFile("https://www.dropbox.com/sh/20lx1e57hh8052f/AABx3tKCNXpctqSGAxk985Q5a?dl=0", "/20171113_172837_v2.jpg",
        //     delegate (float progress)
        // {
        //     Debug.Log(progress*100 + "%");
        // }, delegate(byte[] result)
        // {
        //     Texture2D texture = new Texture2D(1, 1);
        //     texture.LoadImage(result);
        //     texture.Apply();
        //     downloadImage = texture;
        // }, delegate (string error)
        // {
        //     Debug.Log(error);
        // });

        // DropboxAPI.SharingGetSharedLinkMetadata("https://www.dropbox.com/s/qj5ecnxam7ob7rf/20171113_172837.jpg?dl=0", delegate (string result)
        // {
        //     Debug.Log(result);
        // });
        // DropboxAPI.SharingGetSharedLinkMetadata("https://www.dropbox.com/sh/20lx1e57hh8052f/AABx3tKCNXpctqSGAxk985Q5a?dl=0", "/20171113_172837_v2.jpg", delegate (string result)
        // {
        //     Debug.Log(result);
        // });
        // DropboxAPI.SharingGetSharedLinkMetadata("https://www.dropbox.com/s/qj5ecnxam7ob7rf/20171113_172837.jpg?dl=0", delegate (SharingGetSharedLinkMetadataResult result)
        // {
        //      if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.name);
        //     else
        //         Debug.Log(result.error_summary);
        // });
        // DropboxAPI.SharingGetSharedLinkMetadata("https://www.dropbox.com/sh/20lx1e57hh8052f/AABx3tKCNXpctqSGAxk985Q5a?dl=0", "/20171113_172837_v2.jpg", delegate (SharingGetSharedLinkMetadataResult result)
        // {
        //      if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.name);
        //     else
        //         Debug.Log(result.error_summary);
        // });        

        // DropboxAPI.SharingListFileMembers("id:0-KNFfp-7xAAAAAAAAAAIA", delegate (string result)
        // {
        //     Debug.Log(result);
        // });
        // DropboxAPI.SharingListFileMembers("id:0-KNFfp-7xAAAAAAAAAAIA", delegate (SharingListFileMembersResult result)
        // {
        //      if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.users.Count);
        //     else
        //         Debug.Log(result.error_summary);
        // }); 


        // DropboxAPI.SharingListFileMembersBatch(new string[]{"id:0-KNFfp-7xAAAAAAAAAAIA"}, delegate (string result)
        // {
        //     Debug.Log(result);
        // }); 
        // DropboxAPI.SharingListFileMembersBatch(new string[]{"id:0-KNFfp-7xAAAAAAAAAAIA"}, delegate (SharingListFileMembersBatchResult result)
        // {
        //      if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.files.Count);
        //     else
        //         Debug.Log(result.error_summary);
        // }); 

        // DropboxAPI.SharingListFileMembersContinue("<cursor>", delegate(string result)
        // {
        //     Debug.Log(result);
        // });     
        // DropboxAPI.SharingListFileMembersContinue("<cursor>", delegate(SharingListFileMembersContinueResult result)
        // {
        //      if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.cursor);
        //     else
        //         Debug.Log(result.error_summary);
        // });     

        // DropboxAPI.SharingListFolderMembers("84528192421", delegate(string result)
        // {
        //     Debug.Log(result);
        // }); 
        // DropboxAPI.SharingListFolderMembers("84528192421", delegate(SharingListFolderMembersResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.cursor);
        //     else
        //         Debug.Log(result.error_summary);
        // });         
        // DropboxAPI.SharingListFolderMembersContinue("<cursor>", delegate(string result)
        // {
        //     Debug.Log(result);
        // });     
        // DropboxAPI.SharingListFolderMembersContinue("<cursor>", delegate(SharingListFolderMembersContinueResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.cursor);
        //     else
        //         Debug.Log(result.error_summary);
        // });              
        // DropboxAPI.SharingListFolders(delegate (string result)
        // {           
        //     Debug.Log(result);
        // }); 
        // DropboxAPI.SharingListFolders(delegate (SharingListFoldersResult result)
        // {           
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.cursor);
        //     else
        //         Debug.Log(result.error_summary);
        // });         
        // DropboxAPI.SharingListFoldersContinue("<cursor>", delegate(string result)
        // {
        //     Debug.Log(result);
        // });  
        // DropboxAPI.SharingListFoldersContinue("<cursor>", delegate(SharingListFoldersContinueResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.cursor);
        //     else
        //         Debug.Log(result.error_summary);
        // });          
        // DropboxAPI.SharingListMountableFolders(delegate (string result)
        // {
        //     Debug.Log(result);
        // });
        // DropboxAPI.SharingListMountableFolders(delegate (SharingListMountableFoldersResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.cursor);
        //     else
        //         Debug.Log(result.error_summary);
        // });        
        // DropboxAPI.SharingListMountableContinue("<cursor>", delegate (string result)
        // {
        //     Debug.Log(result);
        // });
        // DropboxAPI.SharingListMountableContinue("<cursor>", delegate (SharingListMountableContinueResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.cursor);
        //     else
        //         Debug.Log(result.error_summary);
        // });        
        // DropboxAPI.SharingListReceivedFiles(delegate (string result)
        // {
        //     Debug.Log(result);
        // });
        // DropboxAPI.SharingListReceivedFiles(delegate (SharingListReceivedFilesResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.cursor);
        //     else
        //         Debug.Log(result.error_summary);
        // });        
        // DropboxAPI.SharingListReceivedFilesContinue("<cursor>", delegate (string result)
        // {
        //     Debug.Log(result);
        // }); 
        // DropboxAPI.SharingListReceivedFilesContinue("<cursor>", delegate (SharingListReceivedFilesContinueResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.cursor);
        //     else
        //         Debug.Log(result.error_summary);
        // });         
        // DropboxAPI.SharingListSharedLinks("ZtkX9_EHj3x7PMkVuFIhwKYXEpwpLwyxp9vMKomUhllil9q7eWiAu", delegate (string result)
        // {
        //     Debug.Log(result);
        // }); 
        // DropboxAPI.SharingListSharedLinks("ZtkX9_EHj3x7PMkVuFIhwKYXEpwpLwyxp9vMKomUhllil9q7eWiAu", delegate (SharingListSharedLinksResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.cursor);
        //     else
        //         Debug.Log(result.error_summary);
        // });             
        // DropboxAPI.SharingModifySharedLinkSettings("https://www.dropbox.com/s/qj5ecnxam7ob7rf/20171113_172837.jpg?dl=0", "public", false, delegate (string result)
        // {
        //     Debug.Log(result);
        // });
        // DropboxAPI.SharingModifySharedLinkSettings("https://www.dropbox.com/s/qj5ecnxam7ob7rf/20171113_172837.jpg?dl=0", "public", false, delegate (SharingModifySharedLinkSettingsResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.name);
        //     else
        //         Debug.Log(result.error_summary);
        // });        
        // DropboxAPI.SharingMountFolder("<id>", delegate (string result)
        // {
        //     Debug.Log(result);
        // }); 
        // DropboxAPI.SharingMountFolder("<id>", delegate (SharingMountFolderResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.name);
        //     else
        //         Debug.Log(result.error_summary);
        // });   

        // DropboxAPI.SharingRelinquishFileMembership("<id>", delegate (string result)
        // {
        //     Debug.Log(result);
        // });              
        // DropboxAPI.SharingRelinquishFileMembership("<id>", delegate (SharingRelinquishFileMembershipResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log("DONE");
        //     else
        //         Debug.Log(result.error_summary);
        // }); 

        // DropboxAPI.SharingRelinquishFolderMembership("<id>", false, delegate (string result)
        // {
        //     Debug.Log(result);
        // });              
        // DropboxAPI.SharingRelinquishFolderMembership("<id>", false, delegate (SharingRelinquishFolderMembershipResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log("DONE");
        //     else
        //         Debug.Log(result.error_summary);
        // }); 

        // DropboxAPI.SharingRemoveFileMember2("<fileId>", "<email>", delegate (string result)
        // {
        //     Debug.Log(result);
        // });  
        // DropboxAPI.SharingRemoveFileMember2("<fileId>", "<email>", delegate (SharingRemoveFileMember2Result result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log("DONE");
        //     else
        //         Debug.Log(result.error_summary);
        // });          
        // DropboxAPI.SharingRemoveFolderMember("<fileId>", "<email>", false, delegate (string result)
        // {
        //     Debug.Log(result);
        // });   
        // DropboxAPI.SharingRemoveFolderMember("<fileId>", "<email>", false, delegate (SharingRemoveFolderMemberResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.async_job_id);
        //     else
        //         Debug.Log(result.error_summary);
        // });    

        // DropboxAPI.SharingRevokeSharedLink("https://www.dropbox.com/s/ptspm57lbhnupyt/20171117_105727.jpg?dl=0", delegate (string result)
        // {
        //     Debug.Log(result);
        // });  
        // DropboxAPI.SharingRevokeSharedLink("https://www.dropbox.com/s/ptspm57lbhnupyt/20171117_105727.jpg?dl=0", delegate (SharingRevokeSharedLinkResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log("DONE");
        //     else
        //         Debug.Log(result.error_summary);
        // });   

        // DropboxAPI.SharingSetAccessInheritance("84528192421", "inherit", delegate (string result)
        // {
        //     Debug.Log(result);
        // });  
        // DropboxAPI.SharingSetAccessInheritance("84528192421", "inherit", delegate (SharingSetAccessInheritanceResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.name);
        //     else
        //         Debug.Log(result.error_summary);
        // });         

        // DropboxAPI.SharingShareFolder("/example/workspace", "editors", false, "team", "members", delegate (string result)
        // {
        //     Debug.Log(result);
        // }); 
        // DropboxAPI.SharingShareFolder("/example/workspace", "editors", false, "team", "members", delegate (SharingShareFolderResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.name);
        //     else
        //         Debug.Log(result.error_summary);
        // });         
        // DropboxAPI.SharingTransferFolder("<shared_folder_id>", "<to_dropbox_id>", delegate (string result)
        // {
        //     Debug.Log(result);
        // }); 
        // DropboxAPI.SharingTransferFolder("<shared_folder_id>", "<to_dropbox_id>", delegate (SharingTransferFolderResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log("DONE");
        //     else
        //         Debug.Log(result.error_summary);
        // }); 
        // DropboxAPI.SharingUnmountFolder("<shared_folder_id>", delegate (string result)
        // {
        //     Debug.Log(result);
        // });   
        // DropboxAPI.SharingUnmountFolder("<shared_folder_id>", delegate (SharingUnmountFolderResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log("DONE");
        //     else
        //         Debug.Log(result.error_summary);
        // });
        // DropboxAPI.SharingUnshareFile("<file>", delegate (string result)
        // {
        //     Debug.Log(result);
        // });
        // DropboxAPI.SharingUnshareFile("<file>", delegate (SharingUnshareFileResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log("DONE");
        //     else
        //         Debug.Log(result.error_summary);
        // });        
        // DropboxAPI.SharingUnshareFolder("<shared_folder_id>", false, delegate (string result)
        // {
        //     Debug.Log(result);
        // });
        // DropboxAPI.SharingUnshareFolder("<shared_folder_id>", false, delegate (SharingUnshareFolderResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.tag);
        //     else
        //         Debug.Log(result.error_summary);
        // });        
        // DropboxAPI.SharingUpdateFileMember("id:0-KNFfp-7xAAAAAAAAAAIA", "cchiutse@hotmail.com", "viewer", delegate (string result)
        // {
        //     Debug.Log(result);
        // });   
        // DropboxAPI.SharingUpdateFileMember("id:0-KNFfp-7xAAAAAAAAAAIA", "cchiutse@hotmail.com", "viewer", delegate (SharingUpdateFileMemberResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log("DONE");
        //     else
        //         Debug.Log(result.error_summary);
        // });           
        // DropboxAPI.SharingUpdateFolderMember("id:0-KNFfp-7xAAAAAAAAAAIA", "cchiutse@hotmail.com", "editor", delegate (string result)
        // {
        //     Debug.Log(result);
        // });
        // DropboxAPI.SharingUpdateFolderMember("id:0-KNFfp-7xAAAAAAAAAAIA", "cchiutse@hotmail.com", "editor", delegate (SharingUpdateFolderMemberResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log("DONE");
        //     else
        //         Debug.Log(result.error_summary);
        // });        
        // DropboxAPI.SharingUpdateFolderPolicy("84528192421", "team", "owner", "members", delegate (string result)
        // {
        //     Debug.Log(result);
        // });             
        // DropboxAPI.SharingUpdateFolderPolicy("84528192421", "team", "owner", "members", delegate (SharingUpdateFolderPolicyResult result)
        // {
        //     if (string.IsNullOrEmpty(result.error_summary))
        //         Debug.Log(result.name);
        //     else
        //         Debug.Log(result.error_summary);
        // });
        #endregion
    }

    // Update is called once per frame
    void Update()
    {

    }
}
