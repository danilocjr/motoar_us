﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DropboxKit;

public class CreateSharedLinkDemo : MonoBehaviour {

	// Use this for initialization
	void Start () {
		DropboxAPI.SharingCreateSharedLinkWithSettings("/SampleImage.PNG", delegate(string json)
		{
			Debug.Log(json);
		});
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
