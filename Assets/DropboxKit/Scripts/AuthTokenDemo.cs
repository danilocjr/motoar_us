﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DropboxKit;

public class AuthTokenDemo : MonoBehaviour
{

    [SerializeField]
    GameObject authPanel, inputPanel, menuPanel;

    [SerializeField]
    Text displayname, space;

    [SerializeField]
    FolderView folder;

    // Use this for initialization
    void Start()
    {
        if (DropboxAPI.HasToken())
        {
            authPanel.SetActive(false);
            inputPanel.SetActive(false);
            menuPanel.SetActive(true);
            ShowAccountInfo();
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Authorize()
    {
        DropboxAPI.Oauth2Authorize();
    }

    public void SetAuthorizationCode(InputField input)
    {
        DropboxAPI.Oauth2Token(input.text, delegate (Oauth2TokenResult result)
        {
            if (string.IsNullOrEmpty(result.error))
            {
                inputPanel.SetActive(false);
                menuPanel.SetActive(true);
                ShowAccountInfo();
            }
            else
            {
                Debug.Log(result.error + " - " + result.error_description);
            }
        });
    }

    void ShowAccountInfo()
    {
        DropboxAPI.GetCurrentAccount(delegate (GetCurrentAccountResult result)
        {
            if(string.IsNullOrEmpty(result.error_summary))
                displayname.text = string.Format("Hello, {0}", result.name.display_name);
        });
        DropboxAPI.GetSpaceUsage(delegate (GetSpaceUsageResult result)
        {
            if(string.IsNullOrEmpty(result.error_summary))
                space.text = string.Format("Used: {0}% ({1}/{2})", Mathf.RoundToInt((float)result.used / (float)result.allocation.allocated * 100), result.used, result.allocation.allocated);
        });
        folder.GetList("//");
    }

    public void ChangeUploadScene()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("UploadFileDemo");
    }

    public void ChangeDownloadScene()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("DownloadFileDemo");
    }

    public void Logout()
    {
        DropboxAPI.ClearToken();
        authPanel.SetActive(true);
        inputPanel.SetActive(false);
        menuPanel.SetActive(false);
        displayname.text = "";
        space.text = "";
    }
}
