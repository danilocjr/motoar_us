﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DropboxKit;

public class FolderView : MonoBehaviour
{

    [SerializeField]
    GameObject itemObj;

    [SerializeField]
    Transform viewContent;

    List<GameObject> items;

    FilesListFolderResultEntry backEntry;

    string currentPath;

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    void Awake()
    {
        items = new List<GameObject>();
    }

    // Use this for initialization
    void Start()
    {
        FilesListFolderResultEntry backEntry = new FilesListFolderResultEntry();
    }

    // Update is called once per frame
    void Update()
    {

    }

	public void GetList(FilesListFolderResultEntry entry)
	{
		GetList(entry.path_lower);
	}
    public void GetList(string path)
    {
        currentPath = path;
        DropboxAPI.FilesListFolder(path, delegate (FilesListFolderResult result)
        {
            if (string.IsNullOrEmpty(result.error_summary))
                SetData(result);
            else
                Debug.Log(result.error_summary);
        });
    }

    public void SetData(FilesListFolderResult result)
    {
		ClearTheList();
        // add back button
        if(currentPath != "//")
        {
            string backPath = "//";
            string[] folders = currentPath.Split('/');
            if(folders.Length > 2)
            {
                backPath = currentPath.Substring(0, currentPath.LastIndexOf('/'));
            }
            FilesListFolderResultEntry backEntry = new FilesListFolderResultEntry();
            backEntry.tag = "folder";
            backEntry.name = "<<BACK";
            backEntry.path_lower = backPath;
            result.entries.Insert(0, backEntry);
        }

        for (int i = 0; i < result.entries.Count; i++)
        {
            FilesListFolderResultEntry entry = result.entries[i];
            GameObject item = GameObject.Instantiate(itemObj);
            item.GetComponentInChildren<Text>().text = entry.name;
			if(entry.tag.Contains("folder"))
			{
				Button btn = item.GetComponent<Button>();
				btn.onClick.AddListener(delegate()
				{
					GetList(entry);
				});
			}
            item.transform.SetParent(viewContent);
            item.gameObject.SetActive(true);
            items.Add(item);
        }
    }

	public void ClearTheList()
	{
		for(int i=0; i<items.Count; i++)
		{
			Destroy(items[i].gameObject);
		}
		items.Clear();
	}
}
