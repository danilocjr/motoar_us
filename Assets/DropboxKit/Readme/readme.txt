DropboxKit Readme (v3)

1. Create your dropbox developer account to create apps
https://www.dropbox.com/developers/

2. Drag /DropboxKit/Prefabs/DropboxKit prefab to your scene

3. Fill your dropbox app infomation including App id and App secret.

4. Add using DropboxKit; in your c# file.

5. Make sure after Awake() to call DropboxKit API. (Let API to initialization.)

6. Done.

Dropbox API Document
https://www.dropbox.com/developers/documentation/http/documentation

Demo file in /DropboxKit/Scenes Folder to take reference.
p.s. To run AuthTokenMenu Demo, you should add all demo scene to build settings (scene in build).

If you have any question, please email to cchiutse@hotmail.com